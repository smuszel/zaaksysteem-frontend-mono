const babelLoaderTest = loader => /babel-loader/.test(loader);

const getBabelLoader = rules => {
  const babelLoaderOrOneOf = (babelLoader = rules.find(rule => {
    return rule.oneOf
      ? rule.oneOf.find(({ loader }) => babelLoaderTest(loader))
      : babelLoaderTest(rule.loader);
  }));

  return babelLoaderOrOneOf.oneOf
    ? getBabelLoader(babelLoaderOrOneOf.oneOf)
    : babelLoaderOrOneOf;
};

module.exports = async ({ config, mode }) => {
  const babelLoader = getBabelLoader(config.module.rules);
  babelLoader.include = [babelLoader.include, /apps/, /packages/, /@mintlab/];

  if (mode === 'PRODUCTION') {
    // Disable TypeScript check for static build
    config.plugins = config.plugins.filter(
      plugin => typeof plugin.tsconfig === 'undefined'
    );
  }

  config.node = {
    __dirname: true,
  };

  return config;
};
