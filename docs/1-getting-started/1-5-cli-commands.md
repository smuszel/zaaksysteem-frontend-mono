# CLI commands

## 🤓 Power to the user

> Please don't use `sudo yarn install` or `sudo npm install`, do you really want to give 3rd party developers that power over your system?

On this page you'll find a list of commands and a description of what these commands do.
