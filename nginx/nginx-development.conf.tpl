events {
}

http {

    map $http_x_forwarded_proto $fe_https {
      default off;
      https on;
    }

    server {
        listen 82;


        # Case Management proxy rules
        location /main/sockjs-node {
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_pass   http://PROXY_HOST:3000/sockjs-node;
        }

        location /main {
            proxy_pass   http://PROXY_HOST:3000/;
        }


        # Admin proxy rules
        location /admin/sockjs-node {
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_pass   http://PROXY_HOST:3001/sockjs-node;
        }

        location /admin {
            proxy_pass   http://PROXY_HOST:3001/;
        }

        # PIP proxy rules
        location /my-pip/sockjs-node {
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_pass   http://PROXY_HOST:3002/sockjs-node;
        }

        location /my-pip {
            proxy_pass   http://PROXY_HOST:3002/;
        }
    }
}
