import { push, RouterRootState } from 'connected-react-router';
import { Middleware } from 'redux';
import { batch } from 'react-redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { MiddlewareHelper } from '@zaaksysteem/common/src/types/MiddlewareHelper';
import { APICommunication } from '@zaaksysteem/generated';
import { showSnackbar } from '@zaaksysteem/common/src/store/ui/snackbar/snackbar.actions';
import { hideDialog } from '@zaaksysteem/common/src/store/ui/dialog/dialog.actions';
import {
  ADD_THREAD_TO_CASE_DIALOG,
  IMPORT_MESSAGE_DIALOG,
} from '../components/Dialogs/dialog.constants';
import { SAVE_COMMUNICATION } from './add/communication.add.constants';
import { fetchThreadListAction } from './threadList/communication.threadList.actions';
import {
  fetchThreadAction,
  markMessageAsRead,
} from './thread/communication.thread.actions';
import { CommunicationRootStateType } from './communication.reducer';
import {
  ADD_ATTACHMENT_TO_CASE,
  ADD_THREAD_TO_CASE,
  ADD_SOURCE_FILE_TO_CASE,
  THREAD_FETCH,
  MARK_MESSAGE_AS_READ,
} from './thread/communication.thread.constants';
import { IMPORT_MESSAGE } from './importMessage/communication.importMessage.constants';
import { DELETE_MESSAGE } from './thread/communication.thread.constants';
import { isMessageUnread } from './thread/library/isMessageRead';

const handleSaveCommunicationSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}, APICommunication.CreateContactMomentRequestBody>
> = (store, next, action) => {
  next(action);

  const state = store.getState();
  const {
    communication: {
      context: { rootPath },
    },
    router: {
      location: { pathname },
    },
  } = state;
  const { dispatch } = store;
  const { thread_uuid } = action.payload;
  const url = `${rootPath}/view/${thread_uuid}`;

  batch(() => {
    dispatch(fetchThreadListAction());

    pathname === url
      ? dispatch(fetchThreadAction(thread_uuid))
      : dispatch(push(url));
  });
};

const handleAddAttachmentToCaseSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  dispatch(showSnackbar('attachmentAddedToCase'));
};

const handleAddThreadToCaseSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const state = store.getState();
  const {
    communication: {
      context: { rootPath },
    },
  } = state;
  const { dispatch } = store;

  batch(() => {
    dispatch(showSnackbar('threadAddedToCase'));
    dispatch(fetchThreadListAction());
    dispatch(push(rootPath));
    dispatch(hideDialog(ADD_THREAD_TO_CASE_DIALOG));
  });
};

const handleAddSourceFileToCaseSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  dispatch(showSnackbar('sourceFileAddedToCase'));
};

const handleDeleteMessageSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch, getState } = store;
  const {
    communication: {
      thread: { messages },
      context: { rootPath },
    },
  } = getState();

  batch(() => {
    dispatch(fetchThreadListAction());

    if (messages && messages.length === 0) {
      dispatch(push(rootPath));
    }
  });
};

const handleImportMessageSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  batch(() => {
    dispatch(showSnackbar('messageImported'));
    dispatch(fetchThreadListAction());
    dispatch(hideDialog(IMPORT_MESSAGE_DIALOG));
  });
};

const handleTreadFetchSuccess: MiddlewareHelper<
  CommunicationRootStateType,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const {
    communication: {
      thread: { messages = [] },
      context: { context },
    },
  } = store.getState();
  const { dispatch } = store;
  const unreadMessages = messages
    .filter(message => isMessageUnread(message, context))
    .map(({ id }) => id);

  if (unreadMessages.length > 0) {
    dispatch(
      markMessageAsRead({
        messageUuids: unreadMessages,
      })
    );
  }
};

const handleMarkAsReadSuccess: MiddlewareHelper<
  CommunicationRootStateType,
  AjaxAction<{}>
> = (store, next, action) => {
  const { dispatch } = store;
  next(action);
  dispatch(fetchThreadListAction());
};

/* eslint complexity: [2, 12] */
export const communicationMiddleware: Middleware<
  {},
  CommunicationRootStateType & RouterRootState
> = store => next => action => {
  switch (action.type) {
    case SAVE_COMMUNICATION.SUCCESS:
      return handleSaveCommunicationSuccess(store, next, action);
    case ADD_ATTACHMENT_TO_CASE.SUCCESS:
      return handleAddAttachmentToCaseSuccess(store, next, action);
    case ADD_THREAD_TO_CASE.SUCCESS:
      return handleAddThreadToCaseSuccess(store, next, action);
    case ADD_SOURCE_FILE_TO_CASE.SUCCESS:
      return handleAddSourceFileToCaseSuccess(store, next, action);
    case DELETE_MESSAGE.SUCCESS:
      return handleDeleteMessageSuccess(store, next, action);
    case IMPORT_MESSAGE.SUCCESS:
      return handleImportMessageSuccess(store, next, action);
    case THREAD_FETCH.SUCCESS:
      return handleTreadFetchSuccess(store, next, action);
    case MARK_MESSAGE_AS_READ.SUCCESS:
      return handleMarkAsReadSuccess(store, next, action);
    default:
      return next(action);
  }
};

export default communicationMiddleware;
