import { communication } from './communication.reducer';
import { communicationMiddleware } from './communication.middleware';
import {
  setCommunicationContext,
  CommunicationSetContextActionPayload,
} from './context/communication.context.actions';

export function getCommunicationModule(
  context: CommunicationSetContextActionPayload
) {
  return {
    id: 'communication',
    reducerMap: {
      communication,
    },
    initialActions: [setCommunicationContext(context)],
    middlewares: [communicationMiddleware],
  };
}
