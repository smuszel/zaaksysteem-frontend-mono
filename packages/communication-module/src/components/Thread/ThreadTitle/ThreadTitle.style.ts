import { makeStyles } from '@material-ui/core';

export const useThreadTitleStyle = makeStyles(
  ({ palette: { primary } }: any) => ({
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    title: {
      flexGrow: 1,
    },
    link: {
      textDecoration: 'none',
      color: primary.main,
    },
    tag: {
      backgroundColor: primary.lighter,
      color: primary.main,
    },
  })
);
