import React from 'react';
import { ExternalMessageType } from '../../../types/Message.types';
import ThreadTitle, { ThreadTitlePropsType } from './ThreadTitle';

type PipMessageThreadTitlePropsType = {
  message: ExternalMessageType;
} & Pick<ThreadTitlePropsType, 'context' | 'showLinkToCase'>;

const PipMessageThreadTitle: React.FunctionComponent<PipMessageThreadTitlePropsType> = ({
  message,
  ...rest
}) => {
  return <ThreadTitle {...rest} title={message.subject} />;
};

export default PipMessageThreadTitle;
