import { makeStyles } from '@material-ui/core';

export const useAttachmentListStyle = makeStyles(
  ({ mintlab: { shadows, greyscale } }: any) => ({
    wrapper: {
      marginTop: 30,
      '&>*:not(:first-child)': { marginTop: 15 },
    },
  })
);
