import React from 'react';
//@ts-ignore
import { boolean, text } from '@mintlab/ui/App/story';
import { stories } from '../../../story';
import { ExternalMessageType } from '../../../types/Message.types';
import { PipMessage } from './PipMessage';

export const getMessage = (): ExternalMessageType => ({
  subject: text('Subject', 'Lorum ipsum dolar sit amet'),
  content: text(
    'Content',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum volutpat mauris in turpis rhoncus posuere. Quisque vel mauris ac magna consequat egestas. Quisque ut bibendum arcu. Vivamus in lobortis mauris, quis venenatis nisl. Nam pretium urna et tempus maximus. Curabitur feugiat pellentesque magna, quis lacinia turpis malesuada eget. Mauris eu facilisis metus. Sed purus leo, rutrum in ultrices in, pellentesque sed mi. Phasellus malesuada metus vitae porta tincidunt. Etiam ut sapien sed arcu consequat tempor quis nec mauris. Cras in nulla tincidunt, scelerisque ex at, pellentesque tellus. Nam ac condimentum sem, in fringilla elit. Donec sodales ipsum mi, a volutpat arcu pellentesque a. Integer ac tellus sed eros sodales semper vitae eget diam. Praesent consectetur pretium dui, ut iaculis nunc ullamcorper id. Morbi sit amet ipsum nec erat elementum faucibus sed vel massa.'
  ),
  createdDate: new Date().toISOString(),
  attachments: boolean('Has attachment', false)
    ? [
        {
          downloadUrl: '',
          id: '',
          name: 'Attachment.pdf',
          size: 300,
        },
      ]
    : [],
  messageType: 'email',
  readEmployee: null,
  readPip: null,
  sender: {
    name: 'Foo Bar',
    type: 'employee',
  },
  summary:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum volutpat mauris',
  threadUuid: '',
  id: '',
  type: 'external_message',
  hasSourceFile: boolean('Archivable', false),
});

stories(module, `${__dirname}/PipMessage`, {
  Default() {
    return (
      <PipMessage
        isUnread={boolean('Unread', false)}
        message={getMessage()}
        expanded={true}
      />
    );
  },
  Unread() {
    return (
      <PipMessage isUnread={true} message={getMessage()} expanded={true} />
    );
  },
});
