import { makeStyles } from '@material-ui/core';
import { MessageHeaderPropsType } from './MessageHeader';

export const useMessageHeaderStyle = makeStyles(
  ({ mintlab: { greyscale }, palette: { primary } }: any) => ({
    header: {
      display: 'flex',
      alignItems: 'flex-start',
    },
    icon: {
      marginTop: 5,
      minWidth: 50,
      display: 'flex',
      justifyContent: 'left',
    },
    meta: {
      flex: '1 1 auto',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      minWidth: 0,
    },
    title: {
      display: 'flex',
      justifyContent: 'flex-start',
    },
    titleMain: {
      whiteSpace: 'nowrap',
      fontWeight: ({ isUnread }: MessageHeaderPropsType) =>
        isUnread ? 700 : 500,
    },
    titleSub: { color: greyscale.darker, marginLeft: 10 },
    info: {
      minWidth: 0,
      color: ({ isUnread }: MessageHeaderPropsType) =>
        isUnread ? primary.main : greyscale.darkest,
    },
    right: {
      display: 'flex',
      alignItems: 'center',
      '&>span>svg': {
        paddingBottom: 4,
      },
    },
    date: {
      color: ({ isUnread }: MessageHeaderPropsType) =>
        isUnread ? primary.main : greyscale.darkest,
    },
    link: {
      color: primary.main,

      '&:hover, &:focus': {
        color: primary.darker,
      },
    },
  })
);
