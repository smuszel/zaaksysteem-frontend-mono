import React from 'react';
import {
  FormRendererFormField,
  FormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { flattenField } from '@zaaksysteem/common/src/components/form/library/formHelpers';
//@ts-ignore
import { Body1 } from '@mintlab/ui/App/Material/Typography';
//@ts-ignore
import { Render } from '@mintlab/ui/App/Abstract/Render';
import { GenericMessageFormPropsType } from '../GenericMessageForm.types';

type RenderFieldPropsType = Pick<GenericMessageFormPropsType, 'formName'> & {
  classes: any;
};

const getPlainTextValue = (value: FormValue | FormValue[] | null) => {
  if (!value || (Array.isArray(value) && value.length === 0)) {
    return '-';
  }

  return Array.isArray(value)
    ? value.map(item => flattenField(item)).join(', ')
    : flattenField(value);
};

export const renderPreviewField = ({ classes }: RenderFieldPropsType) => {
  return function RenderFieldComponent({
    name,
    error,
    value,
    touched,
  }: FormRendererFormField) {
    return (
      <div key={name} className={classes.formRow}>
        <Body1 classes={{ root: classes.formValue }}>
          {getPlainTextValue(value)}
        </Body1>
        <Render condition={Boolean(touched) && Boolean(error)}>
          <div className={classes.error}>{error}</div>
        </Render>
      </div>
    );
  };
};
