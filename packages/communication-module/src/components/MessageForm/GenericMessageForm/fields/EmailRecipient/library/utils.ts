import { FetchCaseRolesType } from '@zaaksysteem/common/src/library/requests/fetchCaseRoles';

export const caseRoleValueResolve = ({ subject }: FetchCaseRolesType) => {
  /* Todo: This is a temporary work-arround because the API spec is not properly
   * implemented yet. Remove when MINTY-2950 is resolved */
  return subject &&
    subject.attributes &&
    subject.attributes.contact_information &&
    subject.attributes.contact_information.email
    ? subject.attributes.contact_information.email
    : '';
};
