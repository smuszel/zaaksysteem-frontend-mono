import { makeStyles } from '@material-ui/core';

export const useMessageTabsStyle = makeStyles(
  ({ typography, mintlab: { greyscale } }: any) => ({
    tabsRoot: {
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    tabWrapper: {
      ...typography.title2,
      fontWeight: typography.fontWeightRegular,
    },
    indicator: {},
  })
);
