import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { CommunicationRootStateType } from '../../../store/communication.reducer';
import {
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
} from '../../../library/communicationTypes.constants';
import MessageForm, { MessageFormPropsType } from './MessageForm';

type PropsFromStateType = Pick<
  MessageFormPropsType,
  'canCreate' | 'defaultSubType' | 'rootPath'
>;

type PropsFromDispatchType = Pick<MessageFormPropsType, 'navigateTo'>;

const mapStateToProps = ({
  communication: {
    context: {
      capabilities: { canCreate, canCreatePipMessage, canCreateEmail },
      rootPath,
    },
  },
}: CommunicationRootStateType): PropsFromStateType => {
  const [defaultSubType] = [
    [canCreatePipMessage, TYPE_PIP_MESSAGE],
    [canCreateEmail, TYPE_EMAIL],
  ]
    .filter(([condition]) => condition)
    .map(([, type]) => type);

  return {
    rootPath,
    canCreate,
    defaultSubType: defaultSubType
      ? (defaultSubType as PropsFromStateType['defaultSubType'])
      : undefined,
  };
};

const mapDispatchToProps: PropsFromDispatchType = {
  navigateTo: url => push(url),
};

export default connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(MessageForm);
