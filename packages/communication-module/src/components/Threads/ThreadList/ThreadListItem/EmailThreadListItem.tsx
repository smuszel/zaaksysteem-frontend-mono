import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import ThreadListItem, { ThreadListItemPropsType } from './ThreadListItem';
import ThreadTypeIcon from './ThreadTypeIcon/ThreadTypeIcon';

type EmailThreadListItemPropsType = {
  createdByName: string;
  type: string;
  subject: string;
  messageCount: number;
} & Omit<ThreadListItemPropsType, 'title' | 'tag' | 'icon' | 'subTitle'>;

const EmailThreadListItem: React.FunctionComponent<EmailThreadListItemPropsType> = ({
  createdByName,
  type,
  subject,
  messageCount,
  ...rest
}) => {
  const [t] = useTranslation('communication');
  const title = t('thread.email.title', {
    createdByName,
  });
  const tag = t(`thread.tags.${type}`);
  const iconType =
    messageCount > 1 ? 'threadType.emailThread' : 'threadType.email';

  return (
    <ThreadListItem
      {...rest}
      title={title}
      subTitle={subject}
      typeTag={tag}
      {...addScopeProp('thread', 'email')}
      icon={<ThreadTypeIcon type={iconType} />}
    />
  );
};

export default EmailThreadListItem;
