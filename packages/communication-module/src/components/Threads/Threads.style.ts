import { makeStyles } from '@material-ui/core';

export const useThreadsStyle = makeStyles(
  ({ mintlab: { greyscale }, palette: { primary } }: any) => ({
    wrapper: {
      height: '100%',
      overflowY: 'hidden',
      display: 'flex',
      width: '100%',
      flexDirection: 'column',
      borderRight: `1px solid ${greyscale.dark}`,
    },
    noContentWrapper: {
      padding: 10,
      margin: 10,
      backgroundColor: primary.lighter,
      borderRadius: 5,
    },
    noContentText: {
      color: greyscale.darkest,
    },
    listWrapper: {
      flexGrow: 1,
    },
  })
);
