import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { CommunicationRootStateType } from '../../../store/communication.reducer';
import { importMessage } from '../../../store/importMessage/communication.importMessage.actions';
import ImportMessageDialog, {
  ImportMessageDialogPropsType,
} from './ImportMessageDialog';

type PropsFromStateType = Pick<
  ImportMessageDialogPropsType,
  'caseUuid' | 'busy'
>;

const mapStateToProps = ({
  communication,
}: CommunicationRootStateType): PropsFromStateType => ({
  caseUuid: communication.context.caseUuid || '',
  busy: communication.importMessage.state === AJAX_STATE_PENDING,
});

type PropsFromDispatchType = Pick<ImportMessageDialogPropsType, 'onSubmit'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  onSubmit(values, caseUuid) {
    const action = importMessage({
      file_uuid: (values.file_uuid as NestedFormValue).value.toString(),
      case_uuid: caseUuid,
    });

    dispatch(action as any);
  },
});

const ImportMessageDialogContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(ImportMessageDialog);

export default ImportMessageDialogContainer;
