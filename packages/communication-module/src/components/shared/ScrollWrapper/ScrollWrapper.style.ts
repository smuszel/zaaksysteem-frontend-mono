import { makeStyles } from '@material-ui/core';

export const useScrollWrapperStyle = makeStyles({
  root: {
    width: '100%',
    overflowY: 'auto',
  },
});
