import { request } from '@zaaksysteem/common/src/library/fetch';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';

export const searchContact = (
  keyword: string
): Promise<APICommunication.SearchContactResponseBody> =>
  request(
    'GET',
    buildUrl<APICommunication.SearchContactRequestParams>(
      `/api/v2/communication/search_contact`,
      {
        keyword,
      }
    )
  )
    .then(requestPromise => requestPromise.json())
    .catch(response => Promise.reject(response));

export const fetchCases = (
  contact_uuid: string
): Promise<APICommunication.GetCaseListForContactResponseBody> =>
  request(
    'GET',
    buildUrl<APICommunication.GetCaseListForContactRequestParams>(
      `/api/v2/communication/get_case_list_for_contact`,
      {
        contact_uuid,
      }
    )
  )
    .then(requestPromise => requestPromise.json())
    .catch(response => Promise.reject(response));
