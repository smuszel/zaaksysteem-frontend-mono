import { render, spyOn, spyOnSequence } from '../../test';
import LazyLoader from '.';

/**
 * @test {LazyLoader}
 */
describe('The `LazyLoader` component', () => {
  test('calls the `promise` prop in the `componentDidMount` lifecycle method', () => {
    const then = jest.fn(() => ({
      then,
    }));
    const { instance, props } = render(LazyLoader, {
      promise: jest.fn(() => ({
        then,
      })),
    });

    const assertedCallCount = 1;

    spyOn(props.promise, assertedCallCount);
    spyOnSequence(then, [instance.extract, instance.onResolved]);
  });
});
