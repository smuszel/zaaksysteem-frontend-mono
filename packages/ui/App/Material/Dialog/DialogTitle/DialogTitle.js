import React from 'react';
import MUIDialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/styles';
import { addScopeAttribute, addScopeProp } from '../../../library/addScope';
import Icon from '../../Icon/Icon';
import { H6 } from '../../Typography';
import Button from '../../Button/Button';
import { dialogTitleStyleSheet } from './DialogTitle.style';

/**
 * *Material Design* **DialogTitle**.
 * - facade for *Material-UI* `DialogTitle`
 *
 * @param {Object} props
 * @param {string} [props.icon]
 * @param {string} props.title
 * @param {string} [props.id]
 * @param {string} [props.scope]
 * @param {boolean} [props.elevated]
 * @param {Function} [props.onCloseClick]
 * @return {ReactElement}
 */
/* eslint complexity: [2, 5] */
const DialogTitle = ({
  icon,
  title,
  id,
  scope,
  classes,
  elevated,
  onCloseClick,
}) => (
  <MUIDialogTitle
    id={id}
    disableTypography={true}
    {...addScopeAttribute(scope, 'title')}
    classes={{
      root: elevated ? classes.rootElevated : classes.rootNormal,
    }}
  >
    {icon && (
      <span className={classes.icon}>
        <Icon>{icon}</Icon>
      </span>
    )}

    <H6 classes={{ root: classes.typography }}>{title}</H6>

    {onCloseClick && (
      <div className={classes.closeButton}>
        <Button
          action={onCloseClick}
          presets={['default', 'icon']}
          {...addScopeProp(scope, 'close')}
        >
          close
        </Button>
      </div>
    )}
  </MUIDialogTitle>
);

export default withStyles(dialogTitleStyleSheet)(DialogTitle);
