type OnBreadcrumbClickType = (
  event: React.SyntheticEvent,
  item: BreadcrumbItemType
) => void;

export type BreadcrumbRendererType = (
  props: BreadcrumbItemPropsType
) => React.ReactElement;

export type BreadcrumbItemType = {
  path: string;
  label: string;
};

export type BreadcrumbItemPropsType = {
  item: BreadcrumbItemType;
  index: number;
  classes: any;
  onItemClick?: OnBreadcrumbClickType;
  scope?: string;
};

export type BreadcrumbsPropsType = {
  maxItems?: number;
  items: BreadcrumbItemType[];
  onItemClick?: OnBreadcrumbClickType;
  itemRenderer?: BreadcrumbRendererType;
  lastItemRenderer?: BreadcrumbRendererType;
  scope?: string;
};
