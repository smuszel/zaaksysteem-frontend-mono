# 🔌 `Breadcrumbs` component

> *Material Design* **Breadcrumbs**.

## Example

    <Breadcrumbs
      items={[
        {
          path: '/',
          label: 'Home',
        }, {
          path: '/level',
          label: 'Level',
        }
      ]}
    />

## See also

- [`Breadcrumbs` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Breadcrumbs)

## External resources

- [*Material UI Breadcrumbs*](https://material-ui.com/lab/breadcrumbs/#breadcrumbs)