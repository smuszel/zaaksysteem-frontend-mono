import React from 'react';
import { shallow } from 'enzyme';
import * as iconMap from './library';
import { Icon } from '.';

const { keys } = Object;

const theme = {
  mintlab: {
    icon: {},
  },
};

/**
 * @test {Icon}
 */
describe('The `Icon` component', () => {
  for (const key of keys(iconMap)) {
    test(`can render the \`${key}\` icon`, () => {
      const wrapper = shallow(<Icon theme={theme}>{key}</Icon>);
      const { displayName } = iconMap[key];

      const actual = wrapper.find(displayName).length;
      const expected = 1;

      expect(actual).toBe(expected);
    });
  }
});
