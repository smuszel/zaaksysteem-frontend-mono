import React from 'react';
import LazyLoader from '@mintlab/ui/App/Abstract/LazyLoader';

const LoadableDatePicker = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "ui.datepicker" */
        './DatePicker'
      )
    }
    {...props}
  />
);

export default LoadableDatePicker;
