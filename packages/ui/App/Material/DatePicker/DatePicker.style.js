import { makeStyles } from '@material-ui/core';

export const useDatePickerStyleSheet = makeStyles(({ palette }) => ({
  adornment: {
    color: palette.thundercloud.lightest,
  },
  root: {
    padding: 10,
  },
}));
