import React from 'react';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';
import { fill } from '../../test';
import { Layout } from '.';

/**
 * @test {Layout}
 */
describe('The `Layout` component', () => {
  test('renders correctly', () => {
    const drawer = {
      primary: fill(3, {
        icon: 'add',
      }),
      secondary: fill(2, { icon: 'add' }),
    };

    const component = shallow(
      <Layout classes={{ root: '' }} drawer={drawer} />
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
