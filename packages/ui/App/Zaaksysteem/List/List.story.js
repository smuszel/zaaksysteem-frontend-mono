/* eslint-disable */
import { React, stories } from '../../story';
import { SortableList, CommonList, ListContainer, useList } from '.';

const stores = {
  Sortable: {
    value: [
      { name: 'helium', id: 'He' },
      { name: 'neon', id: 'Ne' },
      { name: 'argon', id: 'Ar' },
      { name: 'krypton', id: 'Kr' },
      { name: 'xenon', id: 'Xe' },
      { name: 'radon', id: 'Rn' },
    ],
  },
  //   Common: { value: [1, 2, 3, 4] },
};

stories(
  module,
  __dirname,
  {
    Sortable({ store, value }) {
      return (
        <ListContainer>
          <SortableList
            renderItem={({ id, name }) => {
              return (
                <div>
                  <h3 style={{ display: 'inline', marginRight: 25 }}>{id}</h3>
                  <span style={{ textTransform: 'capitalize' }}>{name}</span>
                </div>
              );
            }}
            onReorder={nextValue => store.set({ value: nextValue })}
            value={value}
          />
          <strong>the nobility</strong>
        </ListContainer>
      );
    },
    Common: () => {
      const [value, setValue] = React.useState([1, 2, 3, 4, 5]);
      const { removeItem } = useList(value, setValue);
      return (
        <ListContainer>
          <CommonList
            value={value}
            renderItem={(item, index, className) => {
              return (
                <li className={className} id={index}>
                  {item}
                  <button onClick={() => removeItem(index)}>X</button>
                </li>
              );
            }}
          />
          These buttons could be prettier hehe
        </ListContainer>
      );
    },
  },
  stores
);
