import React from 'react';
import { useListContainerStyle } from './List.style';

const ListContainer: React.ComponentType<{}> = ({ children }) => {
  const { listContainer } = useListContainerStyle();
  return (
    <div className={listContainer}>{children}</div>
  ) as React.ReactElement;
};

export default ListContainer;
