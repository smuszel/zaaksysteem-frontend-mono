import React from 'react';
import { addScopeAttribute } from '../../../library/addScope';
import { iterator } from '../library/iterator';
import style from './Cube.module.css';

const LENGTH = 9;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Cube = ({ color, scope }) => (
  <div className={style['cube']} {...addScopeAttribute(scope)}>
    {iterator(LENGTH).map(item => (
      <div
        key={item}
        className={style['square']}
        style={{
          backgroundColor: color,
        }}
      />
    ))}
  </div>
);
