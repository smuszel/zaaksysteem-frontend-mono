import React from 'react';
import { components } from 'react-select';
import CloseIndicator from '../../../Shared/CloseIndicator/CloseIndicator';
import DropdownIndicatorButton from '../../../Shared/DropdownIndicator/DropdownIndicator';

const ClearIndicator = (props: any) => (
  <components.ClearIndicator {...props}>
    <CloseIndicator />
  </components.ClearIndicator>
);

const DropdownIndicator = (props: any) => {
  const { options } = props;
  return Array.isArray(options) && options.length ? (
    <components.DropdownIndicator {...props}>
      <DropdownIndicatorButton />
    </components.DropdownIndicator>
  ) : null;
};

const Control = ({
  selectProps: { startAdornment },
  children,
  ...rest
}: any) => {
  return (
    <components.Control {...rest}>
      {Boolean(startAdornment) && (
        <div className="startAdornment">{startAdornment}</div>
      )}
      {children}
    </components.Control>
  );
};

export const subcomponents = {
  ClearIndicator,
  DropdownIndicator,
  Control,
};
