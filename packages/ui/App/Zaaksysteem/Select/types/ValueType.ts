export type ValueType<T> = {
  label: string;
  alternativeLabels?: string[];
  value: T;
};
