export type V2ServerErrorType = {
  title?: string[];
  detail?: number;
  source?: string;
  code: string;
};

export type V2ServerErrorsType = {
  response: Response;
  errors?: V2ServerErrorType[];
};
