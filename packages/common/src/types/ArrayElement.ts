/**
 * Returns the type of individual elements in an array type
 */
export type ArrayElement<A> = A extends readonly (infer T)[] ? T : never; //test
