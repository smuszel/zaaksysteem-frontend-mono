import { applyTranslations } from './applyTranslations';

const t = str => {
  const mapping = {
    strawberry: 'aardbeien',
    chocolate: 'chocolade',
    vanilla: 'vanille',
  };
  return mapping[str] || str;
};

describe('the `applyTranslations` function', () => {
  test('translates a simple string value', () => {
    const test = 'vanilla';
    const expected = t('vanilla');
    const actual = applyTranslations(test, t);

    expect(actual).toEqual(expected);
  });

  test('translates values from a simple array', () => {
    const testObj = ['vanilla', 'chocolate'];
    const expected = [t('vanilla'), t('chocolate')];
    const actual = applyTranslations(testObj, t);

    expect(actual).toEqual(expected);
  });

  test('handles arrays of translations nested in an object structure', () => {
    const testObj = {
      first: {
        second: {
          third: ['chocolate', 'strawberry', 'vanilla'],
        },
      },
      fourth: [{ label: 'chocolate', value: 1 }],
    };
    const expected = {
      first: {
        second: {
          third: [t('chocolate'), t('strawberry'), t('vanilla')],
        },
      },
      fourth: [{ label: t('chocolate'), value: 1 }],
    };

    const actual = applyTranslations(testObj, t);
    expect(actual).toEqual(expected);
  });

  test('handles translations as values of object properties', () => {
    const testObj = {
      first: {
        second: {
          third: 'strawberry',
          fourth: {
            fifth: 'vanilla',
          },
        },
      },
    };
    const expected = {
      first: {
        second: {
          third: t('strawberry'),
          fourth: {
            fifth: t('vanilla'),
          },
        },
      },
    };

    const actual = applyTranslations(testObj, t);
    expect(actual).toEqual(expected);
  });

  test('does not modify falsy values ', () => {
    const testObj = {
      first: {
        second: null,
        third: undefined,
      },
      fourth: 'chocolate',
    };
    const expected = {
      first: {
        second: null,
        third: undefined,
      },
      fourth: t('chocolate'),
    };

    const actual = applyTranslations(testObj, t);
    expect(actual).toEqual(expected);
  });

  test('does not modify functions', () => {
    const func = () => {};
    const testObj = {
      first: 'chocolate',
      second: func,
    };

    const expected = {
      first: t('chocolate'),
      second: func,
    };

    const actual = applyTranslations(testObj, t);
    expect(actual).toEqual(expected);
  });

  test('does not modify values that have no translation', () => {
    const testObj = {
      first: 'chocolate',
      second: 'cherry',
    };

    const expected = {
      first: t('chocolate'),
      second: t('cherry'),
    };

    const actual = applyTranslations(testObj, t);
    expect(actual).toEqual(expected);
  });
});
