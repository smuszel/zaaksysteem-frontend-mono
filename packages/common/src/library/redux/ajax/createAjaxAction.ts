/* eslint-disable valid-jsdoc */
import { Dispatch } from 'redux';
import { request } from '../../fetch';
import {
  ActionWithPayload,
  ActionPayloadWithResponse,
} from '../../../types/ActionWithPayload';
import { AjaxActionConstants } from './createAjaxConstants';

type CreateAjaxAction = (
  constants: AjaxActionConstants
) => <P, D = any>(data: {
  method: 'GET' | 'POST' | 'PUT';
  url: string;
  data?: D;
  payload?: P;
  headers?: any;
}) => (dispath: Dispatch<AjaxAction>) => Promise<any>;

export interface AjaxAction<R = {}, P = {}>
  extends ActionWithPayload<ActionPayloadWithResponse<P, R>> {
  ajax: boolean;
  error?: boolean;
  statusCode?: number;
}

const parseResponseBody = (response: Response): Promise<any> => {
  let clone = response.clone();

  return response
    .json()
    .then(body => body)
    .catch(() => clone.text().then(body => body));
};

export const createAjaxAction: CreateAjaxAction = constants => ({
  method,
  url,
  data,
  payload,
  headers,
}) => dispatch => {
  dispatch({
    type: constants.PENDING,
    ajax: true,
    payload: {
      ...(payload as any),
    },
  });

  const merge = (body: any) => ({
    ajax: true,
    payload: {
      ...payload,
      response: body,
    },
  });

  return request(method, url, data, headers)
    .then(response => {
      return parseResponseBody(response).then(body => {
        try {
          dispatch({
            ...merge(body),
            type: constants.SUCCESS,
          });
        } catch (error) {
          console.error(error);
        }
      });
    })
    .catch(response => {
      return parseResponseBody(response).then(body => {
        dispatch({
          ...merge(body),
          type: constants.ERROR,
          error: true,
          statusCode: response.status,
        });
      });
    });
};
