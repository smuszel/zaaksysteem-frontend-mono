import React, { useState, ReactChild } from 'react';
import { useTranslation } from 'react-i18next';
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';

type OpenDialogPropsType = {
  message: ReactChild;
  title?: string;
};

export function useErrorDialog(): [
  ReactChild,
  ({ message, title }: OpenDialogPropsType) => any
] {
  const [isOpen, setOpen] = useState(false);
  const [message, setMessage] = useState();
  const [title, setTitle] = useState('');
  const [t] = useTranslation('common');

  const doOpen = ({ message, title }: OpenDialogPropsType) => {
    setMessage(message);
    setTitle(title || t('common:dialog.error.title'));
    setOpen(true);
  };
  const doClose = () => setOpen(false);
  return [
    <Alert
      primaryButton={{ text: t('common:dialog.ok'), action: doClose }}
      key={message}
      onClose={doClose}
      title={title}
      open={isOpen}
    >
      {message}
    </Alert>,
    doOpen,
  ];
}

export default useErrorDialog;
