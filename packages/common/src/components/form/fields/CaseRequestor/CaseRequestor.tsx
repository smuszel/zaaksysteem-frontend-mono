import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import { NestedFormValue } from '../../types/formDefinition.types';
import { useStyles } from './CaseRequestor.styles';
import { CaseRequestorPropsType } from './CaseRequestor.types';
import { fetchRequestor } from './library/utils';

/* eslint complexity: [2, 7] */
const CaseRequestor: React.FunctionComponent<CaseRequestorPropsType> = ({
  config,
  setFieldValue,
  name,
  value,
  setFieldTouched,
}) => {
  const [{ busy, error }, setFetchState] = useState({
    busy: false,
    error: false,
  });
  const classes = useStyles();
  const [t] = useTranslation('common');

  const fetchData = async () => {
    const requestor = await fetchRequestor(config);

    if (requestor) {
      setFieldValue(name, requestor);
      setFieldTouched(name);
      setFetchState({
        busy: false,
        error: false,
      });
    } else {
      setFetchState({
        busy: false,
        error: true,
      });
    }
  };

  useEffect(() => {
    setFetchState({
      busy: true,
      error: false,
    });
    fetchData();
  }, [config]);

  const errorMessage =
    config && config.errorMessage
      ? config.errorMessage
      : t('caseRequestor.error');

  return (
    <div className={classnames(classes.wrapper, error && classes.error)}>
      {busy && t('caseRequestor.loading')}
      {error && errorMessage}
      {(value as NestedFormValue)?.label}
    </div>
  );
};

export default CaseRequestor;
