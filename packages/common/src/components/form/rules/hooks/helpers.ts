//@ts-ignore
import differenceWith from 'lodash/differenceWith';
//@ts-ignore
import isEqual from 'lodash/isEqual';
import { executeRules } from '../library/executeRules';
import {
  FormDefinition,
  FormValuesType,
  PartialFormValuesType,
} from '../../types/formDefinition.types';
import Rule from '../library/Rule';
import mapValuesToFormDefinition from '../../library/mapValuesToFormDefinition';

export function getValuesDiff<Values = any>(
  values: PartialFormValuesType<Values>,
  newValues: PartialFormValuesType<Values>
): PartialFormValuesType<Values> {
  return Object.keys(values).reduce<PartialFormValuesType<Values>>(
    (acc, key) => {
      if (values[key] !== newValues[key]) {
        return {
          ...acc,
          [key]: newValues[key],
        };
      }

      return acc;
    },
    {}
  );
}

export function isFormDefinitionUpdated<Values>(
  formDefinition: FormDefinition<Values>,
  newFormDefinition: FormDefinition<Values>,
  values: FormValuesType<Values>
): boolean {
  const formDefinitionWithValues = mapValuesToFormDefinition<Values>(
    values as PartialFormValuesType<Values>,
    formDefinition
  );

  const formDefinitionDiff = differenceWith(
    formDefinitionWithValues,
    newFormDefinition,
    isEqual
  );

  return formDefinitionDiff.length > 0;
}

export function getFormDefinitionAfterRules<Values>(
  rules: Rule[],
  formDefinition: FormDefinition<Values>,
  values: FormValuesType<Values>
): FormDefinition<Values> {
  const formDefinitionWithValues = mapValuesToFormDefinition<Values>(
    values as PartialFormValuesType<Values>,
    formDefinition
  );

  return rules.length > 0
    ? executeRules(rules, formDefinitionWithValues)
    : formDefinitionWithValues;
}
