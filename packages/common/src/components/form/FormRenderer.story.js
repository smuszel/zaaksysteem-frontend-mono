import React from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { stories } from '@mintlab/ui/App/story';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';

import FormRenderer from './FormRenderer';
import * as fieldTypes from './constants/fieldTypes';
import { Rule, hasValue, hideFields, showFields, setFieldValue } from './rules';

const renderField = ({ FieldComponent, name, ...rest }, errors, touched) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    compact: false,
    name,
    scope: `email-template-form-component-${name}`,
  };

  return (
    <FormControlWrapper
      key={`email-template-form-component-${name}`}
      {...props}
    >
      <FieldComponent {...props} />
      {errors[name] && touched[name] && <p>{errors[name]}</p>}
    </FormControlWrapper>
  );
};

stories(module, __dirname, {
  Default() {
    const formDefinition = [
      {
        title: 'Step 1',
        fields: [
          {
            name: 'test',
            type: fieldTypes.CHOICE_CHIP,
            value: '',
            format: 'text',
            choices: [
              {
                label: 'Choice A',
                value: 'a',
              },
              {
                label: 'Choice B',
                value: 'b',
              },
            ],
            required: true,
          },
          {
            name: 'number',
            type: fieldTypes.TEXT,
            value: 0,
            format: 'number',
            required: true,
            moreThan: 2,
            lessThan: 10,
            placeholder: 'Enter a number bigger than 2 lower than 10',
          },
        ],
      },
      {
        title: 'Step 2',
        fields: [
          {
            name: 'first_name',
            type: fieldTypes.TEXT,
            value: '',
            required: true,
            format: 'text',
            placeholder: 'Enter your first name',
          },
          {
            name: 'last_name',
            type: fieldTypes.TEXT,
            value: '',
            required: true,
            format: 'text',
            placeholder: 'Enter your last name',
          },
          {
            name: 'email_address',
            type: fieldTypes.TEXT,
            value: '',
            required: true,
            format: 'email',
            allowMagicString: true,
            placeholder: 'e.g. example@fake.com',
          },
        ],
      },
      {
        title: 'Step 3',
        fields: [
          {
            name: 'message',
            type: fieldTypes.TEXTAREA,
            value: '',
            required: true,
            format: 'text',
            placeholder:
              'This value is set by rule engine when you fill out the first name field',
            isMultiline: true,
          },
          {
            name: 'multi_value',
            type: fieldTypes.MULTI_VALUE_TEXT,
            value: [],
            required: true,
            minItems: 2,
            format: 'email',
            multiValue: true,
            placeholder: 'Multi value select field',
          },
        ],
      },
      {
        title: 'Step 4',
        fields: [
          {
            name: 'hidden',
            type: fieldTypes.TEXTAREA,
            value: '',
            required: true,
            format: 'text',
            placeholder:
              'This field is shown when you fill out first name using the rule engine',
            isMultiline: true,
            hidden: true,
          },
        ],
      },
    ];

    const rules = [
      new Rule('first_name')
        .when(hasValue)
        .then(showFields(['hidden']))
        .and(setFieldValue('message', 'Filled by rule engine'))
        .else(hideFields(['hidden']))
        .and(setFieldValue('message', '')),
    ];

    return (
      <FormRenderer formDefinition={formDefinition} rules={rules}>
        {({
          fields,
          errors,
          touched,
          handleNextStep,
          handlePreviousStep,
          activeStepValid,
          hasNextStep,
          hasPreviousStep,
          steps,
        }) => {
          return (
            <div>
              <div>
                <strong>Steps</strong>
                <ul>
                  {steps.map(step => (
                    <li
                      key={step.title}
                    >{`${step.title} (valid: ${step.isValid}, active: ${step.isActive}, touched: ${step.isTouched})`}</li>
                  ))}
                </ul>
              </div>

              <div>
                {hasPreviousStep && (
                  <button type="button" onClick={handlePreviousStep}>
                    Previous step
                  </button>
                )}
                {hasNextStep && (
                  <button
                    type="button"
                    disabled={!activeStepValid}
                    onClick={handleNextStep}
                  >
                    Next step
                  </button>
                )}
              </div>

              <div>
                {fields.map(field => renderField(field, errors, touched))}
              </div>
            </div>
          );
        }}
      </FormRenderer>
    );
  },

  AllFields() {
    const formDefinition = [
      {
        name: 'text',
        label: 'TEXT field',
        type: fieldTypes.TEXT,
        value: '',
      },
      {
        name: 'textarea',
        label: 'TEXTAREA field',
        isMultiline: true,
        type: fieldTypes.TEXTAREA,
        value: '',
      },
      {
        name: 'checkbox',
        label: 'CHECKBOX field',
        checked: false,
        applyBackgroundColor: false,
        type: fieldTypes.CHECKBOX,
        value: '',
      },
      {
        name: 'checkbox_group',
        label: 'CHECKBOX_GROUP field',
        choices: [
          { label: 'Option 1', value: 1 },
          { label: 'Option 2', value: 2 },
        ],
        applyBackgroundColor: false,
        type: fieldTypes.CHECKBOX_GROUP,
        value: null,
      },
      {
        name: 'email',
        label: 'EMAIL field',
        type: fieldTypes.EMAIL,
        placeholder: 'Possibly not needed with email validation',
        value: '',
      },
      {
        name: 'location_select',
        label: 'LOCATION_SELECT field',
        type: fieldTypes.LOCATION_SELECT,
        value: '',
      },
      {
        name: 'magicstring',
        label: 'MAGICSTRING field',
        type: fieldTypes.MAGICSTRING,
        value: '',
      },
      {
        name: 'name',
        label: 'NAME field',
        type: fieldTypes.NAME,
        value: '',
      },
      {
        name: 'options',
        label: 'OPTIONS field',
        type: fieldTypes.OPTIONS,
        value: '',
      },
      {
        name: 'product_select',
        label: 'PRODUCT_SELECT field',
        type: fieldTypes.PRODUCT_SELECT,
        value: '',
      },
      {
        name: 'select',
        label: 'SELECT field',
        choices: [
          {
            label: 'test',
            value: 'test',
          },
        ],
        type: fieldTypes.SELECT,
        value: '',
      },
      {
        name: 'multi_value_text',
        label: 'MULTI_VALUE_TEXT field',
        multiValue: true,
        type: fieldTypes.MULTI_VALUE_TEXT,
        value: '',
      },
      {
        name: 'types',
        label: 'TYPES field',
        type: fieldTypes.TYPES,
        value: '',
      },
      {
        name: 'versions',
        label: 'VERSIONS field',
        type: fieldTypes.VERSIONS,
        value: '',
      },
      {
        name: 'magicstring',
        label: 'FILE_SELECT field',
        type: fieldTypes.FILE_SELECT,
        value: '',
      },
      {
        name: 'case_document_select',
        label: 'CASE_DOCUMENT_SELECT field',
        type: fieldTypes.CASE_DOCUMENT_SELECT,
        value: '',
        config: {
          caseUuid: '',
        },
      },
      {
        name: 'flat_value_select',
        label: 'FLATVALUE_SELECT field',
        choices: [
          {
            label: 'test',
            value: 'test',
          },
        ],
        type: fieldTypes.FLATVALUE_SELECT,
        value: '',
      },
      {
        name: 'choice_chip',
        label: 'CHOICE_CHIP field',
        applyBackgroundColor: false,
        type: fieldTypes.CHOICE_CHIP,
        choices: [
          {
            label: 'test',
            value: 'test',
          },
        ],
        value: '',
      },
      {
        name: 'case_finder',
        label: 'CASE_FINDER field',
        type: fieldTypes.CASE_FINDER,
        value: '',
      },
      {
        name: 'date_picker',
        label: 'DATEPICKER field',
        type: fieldTypes.DATEPICKER,
        value: '',
      },
      {
        name: 'contact_finder',
        label: 'CONTACT_FINDER field',
        type: fieldTypes.CONTACT_FINDER,
        value: '',
      },
      {
        name: 'case_role_selector',
        label: 'CASE_ROLE_SELECTOR field',
        type: fieldTypes.CASE_ROLE_SELECTOR,
        value: '',
      },
      {
        name: 'case_requestor',
        label: 'CASE_REQUESTOR field',
        type: fieldTypes.CASE_REQUESTOR,
        value: '',
      },
    ];

    return (
      <FormRenderer formDefinition={formDefinition}>
        {({ fields, errors, touched }) => {
          return (
            <div>
              {fields.map(field => renderField(field, errors, touched))}
            </div>
          );
        }}
      </FormRenderer>
    );
  },
});
