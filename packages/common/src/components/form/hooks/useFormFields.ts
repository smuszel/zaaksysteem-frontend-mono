import { ComponentType } from 'react';
import { FormikProps, FormikErrors } from 'formik';
import {
  FormValuesType,
  FormRendererFormField,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';
import { FormFields } from '../fields';
import { setTouchedAndHandleChange } from '../library/formHelpers';

export type UseFormFieldsReturnType<Values> = FormRendererFormField<Values>[];

type FieldMapType = { [key: string]: ComponentType<any> };

function getError<Values>(
  errors: FormikErrors<FormValuesType<Values>>,
  name: keyof Values
): string | undefined {
  if (typeof errors[name] === 'undefined') {
    return undefined;
  }

  if (typeof errors[name] === 'string') {
    return errors[name] as string;
  }
}

export function useFormFields<Values = any>(
  formDefinitionFields: AnyFormDefinitionField<Values>[],
  values: FormValuesType<Values>,
  formik: FormikProps<FormValuesType<Values>>,
  customFields?: FieldMapType
): UseFormFieldsReturnType<Values> {
  const {
    errors,
    touched,
    handleChange,
    handleBlur,
    setFieldTouched,
    setFieldValue,
  } = formik;

  const Fields: FieldMapType = {
    ...FormFields,
    ...customFields,
  };

  const fields = formDefinitionFields
    .filter(field => !field.hidden)
    .reduce<FormRendererFormField<Values>[]>((acc, field) => {
      if (field.type && Fields[field.type]) {
        const name = field.name;
        const error = touched[name]
          ? getError<Values>(errors, name)
          : undefined;

        return [
          ...acc,
          {
            ...field,
            definition: field,
            FieldComponent: Fields[field.type],
            value: values[name],
            checked: values[name],
            error,
            touched: touched[name] ? true : false,
            key: `field-${name}`,
            refValue:
              typeof field.refValue === 'string'
                ? values[field.refValue]
                : null,
            onChange: setTouchedAndHandleChange({
              setFieldTouched,
              handleChange,
            }),
            onBlur: handleBlur,
            setFieldValue,
            setFieldTouched,
          },
        ];
      }

      return acc;
    }, []);

  return fields;
}
