import { useState } from 'react';
import { FormikErrors, FormikTouched } from 'formik';
import {
  FormDefinition,
  AnyFormDefinitionField,
  FormValuesType,
} from '../types/formDefinition.types';
import {
  isSteppedForm,
  getActiveSteps,
  isStepValid,
  isStepTouched,
} from '../library/formHelpers';

export type FormStepType = {
  title: string;
  description?: string;
  isValid: boolean;
  isActive: boolean;
  isTouched: boolean;
};

type UseFormStepsType<Values> = {
  formDefinition: FormDefinition<Values>;
  errors: FormikErrors<FormValuesType<Values>>;
  touched: FormikTouched<FormValuesType<Values>>;
};

export type UseFormStepsReturnType<Values> = {
  activeFields: AnyFormDefinitionField<Values>[];
  activeStepNumber: number;
  activeStepValid: boolean;
  activeStep: FormStepType;
  steps: FormStepType[];
  hasNextStep: boolean;
  hasPreviousStep: boolean;
  handleNextStep: () => void;
  handlePreviousStep: () => void;
};

export function useFormSteps<Values>({
  formDefinition,
  errors,
  touched,
}: UseFormStepsType<Values>): UseFormStepsReturnType<Values> {
  const [activeStepNumber, setActiveStepNumber] = useState(1);
  const activeStepIndex = activeStepNumber - 1;

  const activeFields = isSteppedForm(formDefinition)
    ? formDefinition[activeStepIndex].fields
    : formDefinition;

  const activeSteps = isSteppedForm(formDefinition)
    ? getActiveSteps(formDefinition)
    : [];

  const numSteps = activeSteps.length;

  const hasNextStep = activeStepNumber + 1 <= numSteps;
  const hasPreviousStep = activeStepNumber - 1 > 0;

  const handleNextStep = () => {
    if (hasNextStep) {
      setActiveStepNumber(activeStepNumber + 1);
    }
  };

  const handlePreviousStep = () => {
    if (hasPreviousStep) {
      setActiveStepNumber(activeStepNumber - 1);
    }
  };

  const steps = isSteppedForm(formDefinition)
    ? activeSteps.map<FormStepType>(
        ({ title, description, fields }, index) => ({
          title,
          description,
          isValid: isStepValid(fields, errors),
          isActive: index === activeStepIndex,
          isTouched: isStepTouched(fields, touched),
        })
      )
    : [];

  const activeStep = steps[activeStepIndex];

  return {
    activeFields,
    activeStepNumber,
    handleNextStep,
    handlePreviousStep,
    hasNextStep,
    hasPreviousStep,
    steps,
    activeStep,
    activeStepValid: isStepValid(activeFields, errors),
  };
}

export default useFormSteps;
