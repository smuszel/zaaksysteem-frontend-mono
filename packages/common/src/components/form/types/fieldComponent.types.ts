import { FormikProps } from 'formik';
import { ComponentType } from 'react';
import {
  AnyFormValueType,
  GenericFormDefinitionFieldAttributes,
  FormValuesType,
  FormDefinitionRefValueResolved,
} from './generic.types';
import { FormDefinitionField } from './fieldDefinition.types';

export type FormFieldPropsType<
  Values = any,
  Config = any,
  ValueType = AnyFormValueType
> = {
  value: ValueType;
  checked: ValueType;
  error?: string;
  touched: boolean;
  refValue?: FormDefinitionRefValueResolved;
  onChange: (event: React.ChangeEvent<any>) => void;
  onBlur: (event: React.SyntheticEvent) => void;
  submit?: () => void;
} & GenericFormDefinitionFieldAttributes<Values, Config, ValueType> &
  Pick<
    FormikProps<FormValuesType<Values>>,
    'setFieldValue' | 'setFieldTouched'
  >;

export interface FormRendererFormField<Values = any>
  extends FormFieldPropsType<Values> {
  definition: FormDefinitionField<Values>;
  FieldComponent: ComponentType<FormFieldPropsType<Values>>;
}

export interface FormRendererFieldComponentPropsType<Values = any> {
  name: keyof Values;
  error?: string;
}
