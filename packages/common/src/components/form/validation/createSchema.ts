import * as yup from 'yup';
import i18next from 'i18next';
import { get } from '@mintlab/kitchen-sink/source';
import {
  FormDefinitionNumberField,
  AnyFormDefinitionField,
  FormDefinitionEmailField,
} from '../types/formDefinition.types';
import { STATUS_FAILED, STATUS_PENDING } from '../constants/fileStatus';
import { getCustomErrorMessages } from './library/getCustomErrorMessages';

/*
 * Matches emailaddresses and magic strings, for example:
 * - test@zaaksysteem.nl
 * - [[ behandelaar_email ]]
 */
const EMAIL_OR_MAGIC_STRING_REGEXP = /^\[\[(\s+)?([a-z0-9_]+)(\s+)?\]\]$|^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export const createNumberSchema = (
  field: FormDefinitionNumberField
): yup.NumberSchema => {
  const setMin = (schema: yup.NumberSchema) =>
    field.min ? schema.min(field.min) : schema;
  const setMax = (schema: yup.NumberSchema) =>
    field.max ? schema.max(field.max) : schema;
  const setLessThan = (schema: yup.NumberSchema) =>
    field.lessThan ? schema.lessThan(field.lessThan) : schema;
  const setMoreThan = (schema: yup.NumberSchema) =>
    field.moreThan ? schema.moreThan(field.moreThan) : schema;

  const [schema] = [yup.number().integer()]
    .map(setMin)
    .map(setMax)
    .map(setLessThan)
    .map(setMoreThan);

  return schema;
};

export const createFileSchema = (t: i18next.TFunction): yup.MixedSchema => {
  const validateFunction = (value: any) =>
    !(
      get(value, 'status') === STATUS_PENDING ||
      get(value, 'status') === STATUS_FAILED
    );

  return yup
    .mixed()
    .test(
      'file-valid',
      t('common:validations.custom.file.invalidFile'),
      validateFunction
    );
};

export const createTextSchema = (
  field: AnyFormDefinitionField
): yup.StringSchema<string | null> => {
  const setMin = (schema: yup.StringSchema<string | null>) =>
    field.min ? schema.min(field.min) : schema;
  const setMax = (schema: yup.StringSchema<string | null>) =>
    field.max ? schema.max(field.max) : schema;

  const [schema] = [
    yup
      .string()
      .trim()
      .nullable(),
  ]
    .map(setMin)
    .map(setMax);

  return schema;
};

export const createMixedSchema = (): yup.MixedSchema => {
  return yup.mixed();
};

export const createEmailSchema = (
  field: FormDefinitionEmailField,
  t: i18next.TFunction
): yup.StringSchema<string | null> => {
  return field.allowMagicString
    ? yup
        .string()
        .nullable()
        .matches(EMAIL_OR_MAGIC_STRING_REGEXP, {
          message: t(
            'formRenderer:validations.custom.string.emailOrMagicString'
          ),
          excludeEmptyString: true,
        })
    : yup
        .string()
        .nullable()
        .email();
};

export function createArraySchema<Schema>(
  field: AnyFormDefinitionField,
  t: i18next.TFunction
): yup.ArraySchema<Schema> | yup.NullableArraySchema<Schema> {
  const errorMessages = getCustomErrorMessages(field, ['min', 'max'], t);

  const setMin = (
    schema: yup.ArraySchema<Schema> | yup.NullableArraySchema<Schema>
  ) =>
    field.minItems ? schema.min(field.minItems, errorMessages.min) : schema;
  const setMax = (
    schema: yup.ArraySchema<Schema> | yup.NullableArraySchema<Schema>
  ) =>
    field.maxItems ? schema.max(field.maxItems, errorMessages.max) : schema;

  const [schema] = [yup.array<Schema>().nullable()].map(setMin).map(setMax);

  return schema;
}
