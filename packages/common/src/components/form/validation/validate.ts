import * as yup from 'yup';
import { FormValue } from '../types/formDefinition.types';

export function validate(
  schema: yup.ObjectSchema<any>,
  values: {
    [key: string]: FormValue | FormValue[];
  }
): Promise<void> {
  return schema.validate(values, { abortEarly: false });
}

export default validate;
