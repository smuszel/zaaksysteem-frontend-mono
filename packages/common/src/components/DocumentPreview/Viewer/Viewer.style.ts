import { makeStyles } from '@material-ui/core/styles';

export const useViewerStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    wrapper: {
      position: 'relative',
      backgroundColor: greyscale.darkest,
      width: '100%',
      overflow: 'hidden',
    },
    pageWrapper: {
      position: 'relative',
      width: '100%',
      height: '100%',
      overflow: 'auto',
    },
    page: {
      display: 'flex',
      justifyContent: 'center',
      '& > .react-pdf__Page__canvas': {
        margin: 10,
      },
    },
    controls: {
      position: 'absolute',
      bottom: 20,
      right: 20,
      zIndex: 1,
    },
  })
);
