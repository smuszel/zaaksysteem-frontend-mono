export type CreateDocumentsDialogPropsType = {
  onConfirm: () => void;
  onClose: () => void;
  open: boolean;
  caseUUID?: string;
  directoryUUID?: string;
};
