import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '../library/createDialogActions';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

/**
 * Alert style Dialog
 *
 * @param {Object} props
 * @param {boolean} [props.open=false]
 * @param {Object} [props.primaryButton]
 * @param {Object} [props.secondaryButton]
 * @param {String} props.title
 * @param {String} [props.scope]
 * @param {any} [props.onClose]
 * @param {React.children} [props.children]
 * @return {ReactElement}
 */
export const Alert = ({
  open = false,
  primaryButton,
  secondaryButton,
  title,
  scope,
  children,
  ...rest
}) => {
  const dialogActions = getDialogActions(primaryButton, secondaryButton, scope);

  return (
    <Dialog aria-label={title} open={open} {...rest}>
      <DialogTitle title={title} scope={scope} />
      <DialogContent>{children}</DialogContent>
      {dialogActions && <DialogActions>{dialogActions}</DialogActions>}
    </Dialog>
  );
};

export default Alert;
