import React, { Fragment } from 'react';
import i18next from 'i18next';
import classNames from 'classnames';
import fecha from 'fecha';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
// @ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
// @ts-ignore
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import { getInfo } from '../../FileExplorer/library/getInfo';
import { DocumentItemType } from '../../FileExplorer/types/FileExplorerTypes';
import { ClassesType } from '../types/types';
import { isDocument } from './isDocument';

type ColumnNames = 'mimeType' | 'modified' | 'download';
type NameColumnType = {
  name: ({
    fileNameAction,
  }: {
    fileNameAction?: (
      item: DocumentItemType
    ) => ((event: React.MouseEvent) => void) | undefined;
  }) => ColumnType;
};
type GetColumnsReturnType = NameColumnType &
  Record<ColumnNames, () => ColumnType>;

export const getColumns = ({
  t,
  classes,
  handleFileOpen,
}: {
  t: i18next.TFunction;
  classes: ClassesType;
  handleFileOpen: (item: DocumentItemType) => void;
}): GetColumnsReturnType => ({
  name: ({ fileNameAction }) => {
    return {
      label: t('DocumentExplorer:columns.name.label'),
      name: 'name',
      width: 1,
      flexGrow: 1,
      flexShrink: 0,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }) => {
        const notAccepted = rowData.type === 'document' && !rowData.accepted;
        const parsedName =
          rowData.name +
          (notAccepted
            ? ` ${t('DocumentExplorer:columns.name.notAccepted')}`
            : '');

        const action = fileNameAction && fileNameAction(rowData);

        return (
          <div className={classes.nameCell}>
            <ZsIcon
              size={38}
              classes={{
                wrapper: classes.iconWrapper,
              }}
            >
              {getInfo(rowData).icon}
            </ZsIcon>
            <Tooltip
              title={parsedName}
              placement={'top-start'}
              enterDelay={700}
            >
              <span
                className={classNames({
                  [classes.notAcceptedLabel]: notAccepted,
                })}
              >
                {action ? (
                  <Button
                    component="a"
                    className={classes.link}
                    action={action}
                  >
                    {parsedName}
                  </Button>
                ) : (
                  <Fragment>{parsedName}</Fragment>
                )}
              </span>
            </Tooltip>
          </div>
        );
      },
    };
  },
  mimeType: () => ({
    label: t('DocumentExplorer:columns.type.label'),
    name: 'mimeType',
    width: 170,
    showFromWidth: 600,
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }) => (
      <div className={classes.metaCell}>
        {t(`DocumentExplorer:fileTypes.${getInfo(rowData).label}`)}
      </div>
    ),
  }),
  modified: () => ({
    label: t('DocumentExplorer:columns.modified.label'),
    name: 'modified',
    width: 120,
    showFromWidth: 650,
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }) => (
      <div className={classes.metaCell}>
        {rowData.modified
          ? fecha.format(
              new Date(rowData.modified),
              t('common:dates.dateFormatTextShort')
            )
          : ''}
      </div>
    ),
  }),
  download: () => ({
    label: t('DocumentExplorer:columns.download.label'),
    name: 'download',
    width: 100,
    disableSort: true,
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }) =>
      isDocument(rowData) ? (
        <Tooltip
          title={t('DocumentExplorer:columns.download.toolTip')}
          placement={'top-start'}
          enterDelay={400}
        >
          <Button
            action={(event: React.MouseEvent) => {
              handleFileOpen(rowData);
              event.preventDefault();
              event.stopPropagation();
            }}
            icon={'save_alt'}
            presets={['primary', 'medium']}
            title={t('DocumentExplorer:columns.download.toolTip')}
          />
        </Tooltip>
      ) : (
        <Fragment />
      ),
  }),
});
