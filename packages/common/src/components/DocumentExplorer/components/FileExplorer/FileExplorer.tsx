import React from 'react';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { isDocument } from '../DocumentExplorer/utils/isDocument';
import {
  FileExplorerPropsType,
  RowClickEvent,
} from './types/FileExplorerTypes';
import { CommonItemType } from './types/FileExplorerTypes';

const FileExplorer: React.ComponentType<FileExplorerPropsType> = ({
  items,
  columns,
  onFileOpen,
  onFolderOpen,
  onRowClick,
  loading = false,
  noRowsMessage,
  ...rest
}) => {
  const handleRowDoubleClick = ({ rowData }: RowClickEvent) => {
    if (isDocument(rowData)) {
      onFileOpen(rowData);
    } else {
      onFolderOpen(rowData);
    }
  };

  const getDefaultSortedRows = () => {
    if (!items || !items.length) return items;

    const defaultSort = (sortA: CommonItemType, sortB: CommonItemType) =>
      sortA.name.localeCompare(sortB.name);

    const directory = items
      .filter((item: CommonItemType) => item.type === 'directory')
      .sort(defaultSort);
    const documents = items
      .filter((item: CommonItemType) => item.type === 'document')
      .sort(defaultSort);

    return [...directory, ...documents];
  };

  return (
    <SortableTable
      rows={getDefaultSortedRows()}
      columns={columns}
      onRowDoubleClick={handleRowDoubleClick}
      onRowClick={onRowClick}
      loading={loading}
      noRowsMessage={noRowsMessage}
      {...rest}
    />
  );
};

export default FileExplorer;
