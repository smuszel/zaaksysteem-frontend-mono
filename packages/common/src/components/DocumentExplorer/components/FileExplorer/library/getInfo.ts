import { ItemType } from '../types/FileExplorerTypes';

/* eslint-disable complexity */
const mimeTypeToInfo = (mimeType: string) => {
  const mimeTypeLower = mimeType ? mimeType.toLowerCase() : '';

  if (/\/pdf$/.test(mimeTypeLower)) return { icon: 'file.pdf', label: 'pdf' };
  if (/\/png$/.test(mimeTypeLower)) return { icon: 'file.png', label: 'image' };
  if (/\/jpe?g$/.test(mimeTypeLower))
    return { icon: 'file.jpg', label: 'image' };
  if (/\/(ms)?word$/.test(mimeTypeLower))
    return { icon: 'file.doc', label: 'document' };
  if (/officedocument.wordprocessingml/.test(mimeTypeLower))
    return { icon: 'file.docx', label: 'document' };
  if (/ms-powerpoint/.test(mimeTypeLower))
    return { icon: 'file.ppt', label: 'presentation' };
  if (/officedocument.presentationml/.test(mimeTypeLower))
    return { icon: 'file.pptx', label: 'presentation' };
  if (/(ms)?excel$/.test(mimeTypeLower))
    return { icon: 'file.xls', label: 'spreadsheet' };
  if (/officedocument.spreadsheetml/.test(mimeTypeLower))
    return { icon: 'file.xlsx', label: 'spreadsheet' };

  return { icon: 'file.default', label: 'default' };
};

export const getInfo = (
  item: ItemType
): {
  icon: string;
  label: string;
} => {
  if (item.type === 'directory') {
    return { icon: 'itemType.folder', label: 'folder' };
  }
  return mimeTypeToInfo(item.mimeType);
};
