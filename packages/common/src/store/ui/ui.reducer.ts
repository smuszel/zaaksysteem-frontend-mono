import { combineReducers, Reducer } from 'redux';
import dialogs, { DialogStateType } from './dialog/dialog.reducer';
import snackbar, { SnackbarStateType } from './snackbar/snackbar.reducer';

export interface UIState {
  dialogs: DialogStateType;
  snackbar: SnackbarStateType;
}

export interface UIRootStateType {
  ui: UIState;
}

export default function createUIReducer(
  reducers: { [key: string]: Reducer } = {}
) {
  return combineReducers({
    ...reducers,
    dialogs,
    snackbar,
  });
}
