// Generated on: Wed Apr 08 2020 15:42:01 GMT+0200 (Central European Summer Time)
// Environment used: https://development.zaaksysteem.nl
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of Document domain.

/* eslint-disable */
export namespace APIDocument {
  export interface GetDocumentRequestParams {
    document_uuid: string;
    [k: string]: any;
  }

  export interface GetDocumentResponseBody {
    data?: {
      type: string;
      id: string;
      meta: {
        last_modified_datetime?: string;
        created_datetime?: string;
        [k: string]: any;
      };
      relationships: {
        /**
         * Case this document is related to
         */
        case?: {
          data?: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta?: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Directory this document is part of (map / folder / directory)
         */
        directory?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data?: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        locked_by?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject created this document
         */
        created_by: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * File this document is related to
         */
        file: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
      attributes: {
        /**
         * Fullname of the file with extension
         */
        name: string;
        /**
         * Mime type of the file
         */
        mimetype: string;
        /**
         * Size of the file
         */
        filesize: number;
        /**
         * md5 of the file
         */
        md5: string;
        /**
         * contains the archive properties of the file.
         */
        archive: {
          type?: 'danslist2018';
          is_archivable?: boolean;
          [k: string]: any;
        };
        /**
         * contains the security properties of the file.
         */
        security: {
          virus_status?: 'pending' | 'ok';
          [k: string]: any;
        };
        /**
         * Accepted status of the document
         */
        accepted: boolean;
        /**
         * Version number, an increasing value of 0-9
         */
        current_version?: number;
        /**
         * Status of the document
         */
        status?: {
          state?: 'pending' | 'active' | 'rejected' | 'deleted';
          pending_type?: 'filename_exists' | 'no_permission';
          /**
           * In case of rejected, this is the reason why
           */
          reason?: string;
          [k: string]: any;
        };
        lock?: {
          /**
           * (e.g. locked) since
           */
          since?: string;
          [k: string]: any;
        };
        metadata?: {
          status?: string;
          origin?: 'inkomend' | 'uitgaand' | 'intern';
          date_origin?: string;
          appearance?: string;
          level_of_trust?: 'todo';
          structure?: string;
          document_category?: 'todo';
          pronom_format?: string;
          description?: string;
          [k: string]: any;
        };
        publish?: {
          status?: boolean;
          destinations?: string[];
          [k: string]: any;
        };
        /**
         * List of tags, in case of zaaksysteem: magic strings
         */
        tags?: string[];
      };
    };
    [k: string]: any;
  }

  export interface CreateDocumentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentRequestBody {
    document_file: string;
    document_uuid: string;
    case_uuid: string;
    directory_uuid: string;
    magic_strings?: string[];
    [k: string]: any;
  }

  export interface CreateFileResponseBody {
    data?: {
      type?: 'file';
      id?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateFileRequestBody {
    file: string;
    file_uuid?: string;
    [k: string]: any;
  }

  export interface CreateDocumentFromAttachmentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentFromAttachmentRequestBody {
    attachment_uuid: string;
  }

  export interface CreateDocumentFromEmailResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentFromEmailRequestBody {
    /**
     * The email UUID.
     */
    email_uuid: string;
    /**
     * The output format of the document, original or pdf
     */
    output_format?: 'original' | 'pdf';
  }

  export interface SearchDocumentRequestParams {
    case_uuid?: string;
    keyword?: string;
    [k: string]: any;
  }

  export interface SearchDocumentResponseBody {
    data: {
      type: string;
      id: string;
      meta: {
        last_modified_datetime?: string;
        created_datetime?: string;
        [k: string]: any;
      };
      relationships: {
        /**
         * Case this document is related to
         */
        case?: {
          data?: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta?: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Directory this document is part of (map / folder / directory)
         */
        directory?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data?: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        locked_by?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject created this document
         */
        created_by: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * File this document is related to
         */
        file: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
      attributes: {
        /**
         * Fullname of the file with extension
         */
        name: string;
        /**
         * Mime type of the file
         */
        mimetype: string;
        /**
         * Size of the file
         */
        filesize: number;
        /**
         * md5 of the file
         */
        md5: string;
        /**
         * contains the archive properties of the file.
         */
        archive: {
          type?: 'danslist2018';
          is_archivable?: boolean;
          [k: string]: any;
        };
        /**
         * contains the security properties of the file.
         */
        security: {
          virus_status?: 'pending' | 'ok';
          [k: string]: any;
        };
        /**
         * Accepted status of the document
         */
        accepted: boolean;
        /**
         * Version number, an increasing value of 0-9
         */
        current_version?: number;
        /**
         * Status of the document
         */
        status?: {
          state?: 'pending' | 'active' | 'rejected' | 'deleted';
          pending_type?: 'filename_exists' | 'no_permission';
          /**
           * In case of rejected, this is the reason why
           */
          reason?: string;
          [k: string]: any;
        };
        lock?: {
          /**
           * (e.g. locked) since
           */
          since?: string;
          [k: string]: any;
        };
        metadata?: {
          status?: string;
          origin?: 'inkomend' | 'uitgaand' | 'intern';
          date_origin?: string;
          appearance?: string;
          level_of_trust?: 'todo';
          structure?: string;
          document_category?: 'todo';
          pronom_format?: string;
          description?: string;
          [k: string]: any;
        };
        publish?: {
          status?: boolean;
          destinations?: string[];
          [k: string]: any;
        };
        /**
         * List of tags, in case of zaaksysteem: magic strings
         */
        tags?: string[];
      };
    }[];
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForCaseRequestParams {
    case_uuid: string;
    filter?: {
      'relationships.parent.id'?: string;
      fulltext?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForCaseResponseBody {
    data?: {
      links: {
        download?: {
          /**
           * Download link for document.
           */
          href: string;
        };
        preview?: {
          /**
           * preview link for document.
           */
          href: string;
          meta: {
            'content-type'?: string;
            [k: string]: any;
          };
        };
      };
      type: string;
      id: string;
      attributes: {
        /**
         * Name of the document_entry
         */
        name: string;
        /**
         * Type of document entry
         */
        entry_type: 'document' | 'directory';
        /**
         * Description about directory entry. Valid only for document.
         */
        description: string | null;
        /**
         * Extension of directory_entry. Valid only for document.
         */
        extension: string | null;
        /**
         * Mimetype of directory_entry. Valid only for document
         */
        mimetype: string | null;
        /**
         * Accepted status of directory_entry. Valid only for document.
         */
        accepted: boolean | null;
        /**
         * Last modified date time. Valid only for document.
         */
        last_modified_date_time: string | null;
      };
      relationships: {
        /**
         * Reference to the directory object when directory_entry is of type directory.
         */
        directory?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Reference to the document object when directory_entry is of type document.
         */
        document?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Case this directory_entry is related to
         */
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Parent directory this document_entry.
         */
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    included?: {
      type: string;
      id: string;
      attributes: {
        name: string;
        [k: string]: any;
      };
      relationships: {
        /**
         * Parent directory.
         */
        parent?: {
          data?: {
            id?: string | null;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    [k: string]: any;
  }

  export interface CreateDocumentFromFileResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentFromFileRequestBody {
    /**
     * UUID of the filestore to which the document is linked.
     */
    file_uuid: string;
    /**
     * UUID of the case to which the document is linked.
     */
    case_uuid?: string;
    /**
     * UUID of the document to be created
     */
    document_uuid?: string;
    /**
     * UUID of the directory in which the document is created.
     */
    directory_uuid?: string;
    /**
     * Magic strings associated with document.
     */
    magic_strings?: string[];
  }

  export interface DownloadDocumentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForIntakeRequestParams {
    filter?: {
      /**
       * Search through document intakes with fulltext
       */
      fulltext?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForIntakeResponseBody {
    data?: {
      links: {
        download?: {
          /**
           * Download link for document.
           */
          href: string;
        };
        preview?: {
          /**
           * preview link for document.
           */
          href: string;
          meta: {
            'content-type'?: string;
            [k: string]: any;
          };
        };
      };
      type: string;
      id: string;
      attributes: {
        /**
         * Name of the document_entry
         */
        name: string;
        /**
         * Type of document entry
         */
        entry_type: 'document' | 'directory';
        /**
         * Description about directory entry. Valid only for document.
         */
        description: string | null;
        /**
         * Extension of directory_entry. Valid only for document.
         */
        extension: string | null;
        /**
         * Mimetype of directory_entry. Valid only for document
         */
        mimetype: string | null;
        /**
         * Accepted status of directory_entry. Valid only for document.
         */
        accepted: boolean | null;
        /**
         * Last modified date time. Valid only for document.
         */
        last_modified_date_time: string | null;
      };
      relationships: {
        /**
         * Reference to the directory object when directory_entry is of type directory.
         */
        directory?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Reference to the document object when directory_entry is of type document.
         */
        document?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Case this directory_entry is related to
         */
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Parent directory this document_entry.
         */
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    included?: {
      type: string;
      id: string;
      attributes: {
        name: string;
        [k: string]: any;
      };
      relationships: {
        /**
         * Parent directory.
         */
        parent?: {
          data?: {
            id?: string | null;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    [k: string]: any;
  }

  export interface PreviewDocumentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface Document {
    type: string;
    id: string;
    meta: {
      last_modified_datetime?: string;
      created_datetime?: string;
      [k: string]: any;
    };
    relationships: {
      /**
       * Case this document is related to
       */
      case?: {
        data?: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta?: {
          display_number?: number;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Directory this document is part of (map / folder / directory)
       */
      directory?: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject making the last change to this document
       */
      modified_by?: {
        data?: {
          id: string;
          type: string;
          [k: string]: any;
        };
        meta?: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject making the last change to this document
       */
      locked_by?: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        meta?: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject created this document
       */
      created_by: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        meta?: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * File this document is related to
       */
      file: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
    attributes: {
      /**
       * Fullname of the file with extension
       */
      name: string;
      /**
       * Mime type of the file
       */
      mimetype: string;
      /**
       * Size of the file
       */
      filesize: number;
      /**
       * md5 of the file
       */
      md5: string;
      /**
       * contains the archive properties of the file.
       */
      archive: {
        type?: 'danslist2018';
        is_archivable?: boolean;
        [k: string]: any;
      };
      /**
       * contains the security properties of the file.
       */
      security: {
        virus_status?: 'pending' | 'ok';
        [k: string]: any;
      };
      /**
       * Accepted status of the document
       */
      accepted: boolean;
      /**
       * Version number, an increasing value of 0-9
       */
      current_version?: number;
      /**
       * Status of the document
       */
      status?: {
        state?: 'pending' | 'active' | 'rejected' | 'deleted';
        pending_type?: 'filename_exists' | 'no_permission';
        /**
         * In case of rejected, this is the reason why
         */
        reason?: string;
        [k: string]: any;
      };
      lock?: {
        /**
         * (e.g. locked) since
         */
        since?: string;
        [k: string]: any;
      };
      metadata?: {
        status?: string;
        origin?: 'inkomend' | 'uitgaand' | 'intern';
        date_origin?: string;
        appearance?: string;
        level_of_trust?: 'todo';
        structure?: string;
        document_category?: 'todo';
        pronom_format?: string;
        description?: string;
        [k: string]: any;
      };
      publish?: {
        status?: boolean;
        destinations?: string[];
        [k: string]: any;
      };
      /**
       * List of tags, in case of zaaksysteem: magic strings
       */
      tags?: string[];
    };
  }

  export interface File {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      table_id?: number;
      [k: string]: any;
    };
    relationships?: {
      /**
       * Document this storage item links to, if available
       */
      document?: {
        [k: string]: any;
      };
      /**
       * Related thumbnail
       */
      thumbnail?: {
        [k: string]: any;
      };
      /**
       * Subject created this entry in the file
       */
      created_by?: {
        [k: string]: any;
      };
      [k: string]: any;
    };
    attributes?: {
      /**
       * Fullname of the file with extension
       */
      name?: string;
      /**
       * Name without extension
       */
      basename?: string;
      /**
       * Extension of this file
       */
      extension?: {
        [k: string]: any;
      };
      hash?: {
        type?: 'md5';
        value?: string;
        [k: string]: any;
      };
      archive?: {
        type?: 'danslist2018';
        is_archivable?: boolean;
        [k: string]: any;
      };
      security?: {
        virus_status?: 'pending' | 'ok';
        [k: string]: any;
      };
      filesize?: number;
      mimetype?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DirectoryEntry {
    links: {
      download?: {
        /**
         * Download link for document.
         */
        href: string;
      };
      preview?: {
        /**
         * preview link for document.
         */
        href: string;
        meta: {
          'content-type'?: string;
          [k: string]: any;
        };
      };
    };
    type: string;
    id: string;
    attributes: {
      /**
       * Name of the document_entry
       */
      name: string;
      /**
       * Type of document entry
       */
      entry_type: 'document' | 'directory';
      /**
       * Description about directory entry. Valid only for document.
       */
      description: string | null;
      /**
       * Extension of directory_entry. Valid only for document.
       */
      extension: string | null;
      /**
       * Mimetype of directory_entry. Valid only for document
       */
      mimetype: string | null;
      /**
       * Accepted status of directory_entry. Valid only for document.
       */
      accepted: boolean | null;
      /**
       * Last modified date time. Valid only for document.
       */
      last_modified_date_time: string | null;
    };
    relationships: {
      /**
       * Reference to the directory object when directory_entry is of type directory.
       */
      directory?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Reference to the document object when directory_entry is of type document.
       */
      document?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Case this directory_entry is related to
       */
      case?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta: {
          display_number?: number;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Parent directory this document_entry.
       */
      parent?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject making the last change to this document
       */
      modified_by?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
  }

  export interface Directory {
    type: string;
    id: string;
    attributes: {
      name: string;
      [k: string]: any;
    };
    relationships: {
      /**
       * Parent directory.
       */
      parent?: {
        data?: {
          id?: string | null;
          type?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
  }
}
