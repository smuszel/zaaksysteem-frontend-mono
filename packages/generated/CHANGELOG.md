# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.13.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.13.0...@zaaksysteem/generated@0.13.1) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.12.0...@zaaksysteem/generated@0.13.0) (2020-04-02)


### Features

* **Communication:** MINTY-3411 - save Emails to PDF in Documents ([08046ce](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/08046ce))
* **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))





# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.11.0...@zaaksysteem/generated@0.12.0) (2020-04-01)


### Features

* **ObjectView:** Implement first iteration of Object View ([daa2da5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/daa2da5))





# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.10.2...@zaaksysteem/generated@0.11.0) (2020-03-31)


### Features

* **ObjectTypeManagement:** MINTY-3519 Implement add object type action ([a1df778](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a1df778))





## [0.10.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.10.1...@zaaksysteem/generated@0.10.2) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.10.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.10.0...@zaaksysteem/generated@0.10.1) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.9.0...@zaaksysteem/generated@0.10.0) (2020-03-19)


### Features

* **Catalog:** MINTY-3344 - Add support for custom object types ([55ee940](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/55ee940))





# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.8.1...@zaaksysteem/generated@0.9.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))





## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.8.0...@zaaksysteem/generated@0.8.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.3...@zaaksysteem/generated@0.8.0) (2020-03-05)


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **CaseDocuments:** MINTY-2882 - enable download link ([09f2ce5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/09f2ce5))
* **CaseDocuments:** MINTY-2884 - add search functionality ([f91aea4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f91aea4))
* **CaseDocuments:** MINTY-2995 - enable creating of documents from file uploads ([2792d26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2792d26))
* **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))





## [0.7.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.2...@zaaksysteem/generated@0.7.3) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.1...@zaaksysteem/generated@0.7.2) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.0...@zaaksysteem/generated@0.7.1) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/generated

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.6.1...@zaaksysteem/generated@0.7.0) (2020-02-06)

### Features

- **Communication:** MINTY-2955 Expand unread messages in thread and mark unread messages as read ([f157214](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f157214))
- **Communication:** MINTY-2955 Show read/unread status of thread messages ([9e8ec85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9e8ec85))

## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.6.0...@zaaksysteem/generated@0.6.1) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/generated

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.5.0...@zaaksysteem/generated@0.6.0) (2020-01-22)

### Features

- **MessageForm:** Replace upload ability with ability to select case documents in case context ([e1d1f92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1d1f92))

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.4.0...@zaaksysteem/generated@0.5.0) (2020-01-10)

### Features

- **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.3.0...@zaaksysteem/generated@0.4.0) (2020-01-09)

### Bug Fixes

- **Tasks:** MINTY-2816 - change get_task_list endpoint to filter ([f127e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f127e36))
- **Tasks:** MINTY-2822 - fix displayname + typing issues ([6f5c722](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f5c722))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.2.0...@zaaksysteem/generated@0.3.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **Thread:** Fix search case API to new version as filter-status ([85aff79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85aff79))

### Features

- **AddThread:** Filter out resolved cases ([7f31864](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f31864))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))

# 0.2.0 (2019-10-17)

### Bug Fixes

- add empty attachments to save, remove Document domain from generated ([8cbda28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8cbda28))

### Features

- **GeneratedTypes:** Generated types are now stored in git and manually regenerated ([e6a96b7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e6a96b7))
- **GeneratedTypes:** Throw error when generation fails and disable Document schema since it's not being used and is causing conflicts ([80e5242](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/80e5242))
