import { IModule } from 'redux-dynamic-modules';
import {
  session,
  SessionRootStateType,
} from '@zaaksysteem/common/src/store/session/session.reducer';
import { sessionMiddleware } from '@zaaksysteem/common/src/store/session/session.middleware';
import { fetchSession } from '@zaaksysteem/common/src/store/session/session.actions';

export const getSessionModule = (): IModule<SessionRootStateType> => ({
  id: 'session',
  reducerMap: {
    session,
  },
  //@ts-ignore
  initialActions: [fetchSession()],
  middlewares: [sessionMiddleware],
});

export default getSessionModule;
