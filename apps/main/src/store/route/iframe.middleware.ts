import { LOCATION_CHANGE } from 'connected-react-router';
import { Middleware } from 'redux';

export const iframeMiddleware: Middleware = () => next => action => {
  if (action.type === LOCATION_CHANGE && window.top) {
    const { pathname } = action.payload.location;

    window.top.postMessage(
      {
        type: 'locationChange',
        data: pathname,
      },
      '*'
    );
  }

  return next(action);
};

export default iframeMiddleware;
