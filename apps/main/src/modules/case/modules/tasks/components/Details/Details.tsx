import React from 'react';
import FormRenderer from '@zaaksysteem/common/src/components/form/FormRenderer';
//@ts-ignore
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import ContactFinder from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder';
import {
  FormDefinition,
  FormFieldPropsType,
  FormRendererRenderProps,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import translateFormDefinition from '@zaaksysteem/common/src/components/form/library/translateFormDefinition';
//@ts-ignore
import { Button } from '@mintlab/ui/App/Material/Button';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
import { CaseTask } from '../../types/List.types';
import { useTasksStyles } from '../Tasks.style';
import { useDetailStyles } from './Details.style';
import Toolbar from './Toolbar/Toolbar';

const fieldComponents = {
  contactFinder: ContactFinder,
};

export type DetailsPropsType = {
  formDefinition: FormDefinition;
  editAction: (values: Partial<CaseTask>) => void;
  setCompletionAction: (completed: boolean) => void;
  deleteAction: () => void;
  rootPath: string;
} & Pick<CaseTask, 'is_editable' | 'can_set_completion'>;

export const Details: React.ComponentType<DetailsPropsType> = ({
  formDefinition,
  editAction,
  setCompletionAction,
  deleteAction,
  rootPath,
  is_editable,
  can_set_completion,
}) => {
  const classes = useDetailStyles();
  const tasksClasses = useTasksStyles();
  const [t] = useTranslation();

  return (
    <FormRenderer
      isInitialValid={true}
      enableReinitialize={true}
      formDefinition={translateFormDefinition(formDefinition, t)}
      fieldComponents={fieldComponents}
    >
      {({
        fields,
        isValid,
        values,
        setFieldValue,
      }: FormRendererRenderProps) => {
        return (
          <div className={tasksClasses.wrapperDetails}>
            <Toolbar
              deleteAction={deleteAction}
              setCompletionAction={setCompletionAction}
              rootPath={rootPath}
              isValid={isValid}
              is_editable={is_editable}
              can_set_completion={can_set_completion}
              completed={values.completed as any}
              t={t}
            />

            <div className={classes.scrollWrapper}>
              <PlusButtonSpaceWrapper>
                {fields.map(
                  ({
                    FieldComponent,
                    name,
                    error,
                    touched,
                    value,
                    ...rest
                  }: FormFieldPropsType) => {
                    const restValues = {
                      ...cloneWithout(
                        rest,
                        'setFieldValue',
                        'refValue',
                        'setFieldTouched',
                        'type',
                        'classes'
                      ),
                      disabled: !is_editable || values.completed,
                      ...(name === 'due_date' && {
                        onClose: () => setFieldValue(name, null),
                      }),
                    };

                    return (
                      <FormControlWrapper
                        {...restValues}
                        compact={true}
                        classes={{
                          wrapper: classes.formControlWrapper,
                        }}
                        error={error}
                        touched={touched}
                        key={name}
                      >
                        <FieldComponent
                          name={name}
                          value={value}
                          key={name}
                          {...restValues}
                        />
                      </FormControlWrapper>
                    );
                  }
                )}

                {values.completed && can_set_completion && (
                  <Button
                    presets={['contained', 'medium', 'primary']}
                    action={() => setCompletionAction(false)}
                    disabled={!isValid}
                    fullWidth
                  >
                    {t('tasks:form.reOpen')}
                  </Button>
                )}

                {!values.completed && is_editable && (
                  <Button
                    presets={['contained', 'medium', 'primary']}
                    action={() => editAction(values)}
                    disabled={!isValid}
                    fullWidth
                  >
                    {t('common:dialog.save')}
                  </Button>
                )}
              </PlusButtonSpaceWrapper>
            </div>
          </div>
        );
      }}
    </FormRenderer>
  );
};
