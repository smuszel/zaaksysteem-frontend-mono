import { APICaseManagement } from '@zaaksysteem/generated';

type Attributes = Pick<
  APICaseManagement.Task['attributes'],
  'completed' | 'description' | 'title'
>;

type Meta = Pick<
  APICaseManagement.Task['meta'],
  'is_editable' | 'can_set_completion'
>;

export type CaseTask = {
  task_uuid: string;
  assignee: null | { label: string; value: string };
  due_date: string | null;
} & Attributes &
  Meta;
