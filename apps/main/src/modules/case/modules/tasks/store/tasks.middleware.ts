import { push, RouterRootState } from 'connected-react-router';
import { Middleware } from 'redux';
import { MiddlewareHelper } from '@zaaksysteem/common/src/types/MiddlewareHelper';
import { TasksRootStateType } from './tasks.reducer';
import {
  TASK_CREATE,
  TASK_EDIT,
  TASK_DELETE,
  TASK_SET_COMPLETE_STATUS,
} from './tasks/tasks.constants';
import { fetchTasksAction } from './tasks/tasks.actions';
import { refreshTasks } from './library/library';

const handleTaskChange: MiddlewareHelper<TasksRootStateType &
  RouterRootState> = (store, next, action) => (redirect: boolean) => {
  const state = store.getState();
  const { dispatch } = store;

  next(action);
  refreshTasks();

  if (redirect) {
    dispatch(push(state.tasks.context.rootPath));
  }
};

const handleTaskError: MiddlewareHelper<TasksRootStateType &
  RouterRootState> = (store, next, action) => {
  const {
    tasks: { context },
  } = store.getState();
  const { dispatch } = store;

  next(action);
  dispatch(fetchTasksAction(context));
  refreshTasks();
};

/* eslint complexity: [2, 9] */
export const tasksMiddleware: Middleware<
  {},
  TasksRootStateType & RouterRootState
> = store => next => action => {
  switch (action.type) {
    case TASK_CREATE.PENDING:
    case TASK_SET_COMPLETE_STATUS.PENDING:
      return handleTaskChange(store, next, action)(false);
    case TASK_EDIT.PENDING:
    case TASK_DELETE.PENDING:
      return handleTaskChange(store, next, action)(true);
    case TASK_CREATE.ERROR:
    case TASK_EDIT.ERROR:
    case TASK_DELETE.ERROR:
    case TASK_SET_COMPLETE_STATUS.ERROR:
      return handleTaskError(store, next, action);
    default:
      return next(action);
  }
};
