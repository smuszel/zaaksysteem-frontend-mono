import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CommunicationModule from '@zaaksysteem/communication-module/src';

const CustomerContact = () => {
  return (
    <Switch>
      <Route
        path={`/:prefix/customer-contact`}
        render={({ match }) => (
          <CommunicationModule
            capabilities={{
              allowSplitScreen: true,
              canAddAttachmentToCase: false,
              canAddSourceFileToCase: false,
              canAddThreadToCase: true,
              canCreateContactMoment: false,
              canCreatePipMessage: false,
              canCreateEmail: false,
              canCreateNote: false,
              canCreateMijnOverheid: false,
              canDeleteMessage: true,
              canImportMessage: false,
              canSelectCase: false,
              canSelectContact: false,
              canFilter: true,
              canOpenPDFPreview: true,
            }}
            context="inbox"
            rootPath={match.url}
          />
        )}
      />
    </Switch>
  );
};

export default CustomerContact;
