import { Reducer } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { APICaseManagement } from '@zaaksysteem/generated';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { ObjectType } from '../../types/ObjectView.types';
import { FETCH_OBJECT } from './object.constants';
import { FetchObjectActionPayloadType } from './object.actions';

export interface ObjectStateType {
  object?: ObjectType;
  state: AjaxState;
}

export interface ObjectStateType {
  state: AjaxState;
}

const initialState: ObjectStateType = {
  state: AJAX_STATE_INIT,
};

const handleObjectFetchSuccess = (
  state: ObjectStateType,
  action: AjaxAction<
    APICaseManagement.GetCustomObjectResponseBody,
    FetchObjectActionPayloadType
  >
): ObjectStateType => {
  const object = action.payload.response.data;

  if (!object) {
    return state;
  }

  const {
    id,
    attributes: { custom_fields, date_created, last_modified, status, title },
  } = object;

  return {
    ...state,
    object: {
      uuid: id,
      customFields: custom_fields,
      lastModified: last_modified,
      registrationDate: date_created,
      title: title || '',
      status,
    },
  };
};

const handleObjectViewStateChange = handleAjaxStateChange(FETCH_OBJECT);

export const objectReducer: Reducer<ObjectStateType, AjaxAction<unknown>> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FETCH_OBJECT.PENDING:
    case FETCH_OBJECT.ERROR:
      return handleObjectViewStateChange(state, action as AjaxAction<{}>);
    case FETCH_OBJECT.SUCCESS:
      return handleObjectViewStateChange(
        handleObjectFetchSuccess(
          state,
          action as AjaxAction<
            APICaseManagement.GetCustomObjectResponseBody,
            FetchObjectActionPayloadType
          >
        ),
        action
      );

    default:
      return state;
  }
};

export default objectReducer;
