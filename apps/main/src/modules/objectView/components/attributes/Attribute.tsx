import React from 'react';
import { useAttributeStyles } from './Attribute.style';

export type AttributePropsType = {
  label: string;
  value: string;
};

const Attribute: React.FunctionComponent<AttributePropsType> = ({
  label,
  value,
}) => {
  const classes = useAttributeStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.label}>{label}</div>
      <div className={classes.value}>{value}</div>
    </div>
  );
};

export default Attribute;
