import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { objectStateSelector } from '../store/selectors/objectStateSelector';
import { ObjectType } from '../types/ObjectView.types';
import {
  BreadcrumbBar,
  BreadcrumbBarPropsType,
} from '../../../components/BreadcrumbBar/BreadcrumbBar';
import { useObjectViewStyles } from './ObjectView.style';
import Attributes from './attributes/Attributes';

export interface ObjectViewPropsType {
  object?: ObjectType;
  loading: boolean;
}

const ObjectView: React.FunctionComponent = () => {
  const { object, state: objectViewState } = useSelector(objectStateSelector);
  const classes = useObjectViewStyles();
  const [t] = useTranslation('main');

  if (objectViewState === AJAX_STATE_PENDING || !object) {
    return <Loader />;
  }

  const breadcrumbs: BreadcrumbBarPropsType['breadcrumbs'] = [
    {
      label: t('modules.dashboard'),
      path: '/intern',
    },
    {
      label: object.title,
      path: '',
    },
  ];

  return (
    <div className={classes.wrapper}>
      <BreadcrumbBar breadcrumbs={breadcrumbs} />
      <Attributes object={object} />
    </div>
  );
};

export default ObjectView;
