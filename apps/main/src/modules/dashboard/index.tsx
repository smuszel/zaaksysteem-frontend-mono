import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getDashboardModule } from './store/dashboard.module';
import DashboardContainer from './components/DashboardContainer';

export interface DashboardModulePropsType {
  userId: string;
}

const DashboardModule: React.ComponentType<DashboardModulePropsType> = ({
  userId,
}) => {
  return (
    <DynamicModuleLoader modules={[getDashboardModule(userId)]}>
      <DashboardContainer />
    </DynamicModuleLoader>
  );
};
export default DashboardModule;
