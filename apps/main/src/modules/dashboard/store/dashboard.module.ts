import { IModule } from 'redux-dynamic-modules';
import { dashboardReducer, DashboardRootStateType } from './dashboard.reducer';
import { fetchOpenCases } from './dashboard.actions';

export function getDashboardModule(
  userId: string
): IModule<DashboardRootStateType> {
  return {
    id: 'dashboard',
    reducerMap: {
      dashboard: dashboardReducer as any,
    },
    initialActions: [fetchOpenCases(userId) as any],
  };
}
