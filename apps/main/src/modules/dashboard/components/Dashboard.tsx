import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import { DashboardCaseType } from '../types/Dashboard.types';
import { useDashboardStyles } from './Dashboard.style';
import { CaseDocuments } from './CaseDocuments/CaseDocuments';

export interface DashboardPropsType {
  cases: DashboardCaseType[];
  loading: boolean;
  contacts: {
    [id: string]: string;
  };
  showSnack: () => void;
}

const Dashboard: React.FunctionComponent<DashboardPropsType> = ({
  cases,
  loading,
  contacts,
  showSnack,
}) => {
  const [, setMessages] = useMessages();
  const classes = useDashboardStyles();

  useEffect(() => {
    setMessages({
      'test-message': 'Test snackbar message',
    });
  }, []);

  if (loading) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <div className={classes.item}>
        <h2 className={classes.itemTitle}>UI</h2>
        <div className={classes.itemContent}>
          <Button presets={['primary', 'contained']} action={showSnack}>
            Show test snack
          </Button>
        </div>
      </div>

      <div className={classes.item}>
        <h2 className={classes.itemTitle}>Communcation module</h2>
        <div className={classes.itemContent}>
          <h3 className={classes.itemSectionTitle}>Case communication</h3>
          <ul>
            {cases.map(item => (
              <li key={item.id}>
                <Link to={`/main/case/${item.id}/communication`}>
                  Case {item.caseNumber} - {item.caseType}
                </Link>
              </li>
            ))}
          </ul>
          <h3 className={classes.itemSectionTitle}>Contact communication</h3>
          <ul>
            {Object.entries(contacts).map(([id, name]) => (
              <li key={id}>
                <Link to={`/main/contact/${id}/communication`}>{name}</Link>
              </li>
            ))}
          </ul>
          <h3 className={classes.itemSectionTitle}>
            Customer Contact communication
          </h3>
          <Link to={`/main/customer-contact/`}>Go to customer contact</Link>
        </div>
      </div>

      <div className={classes.item}>
        <h2 className={classes.itemTitle}>Task module</h2>
        <div className={classes.itemContent}>
          <ul>
            {cases.map(item => (
              <li key={item.id}>
                <Link to={`/main/case/${item.id}/fase/tasks`}>
                  Case {item.caseNumber} - {item.caseType}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>

      <div className={classes.item}>
        <h2 className={classes.itemTitle}>Document preview</h2>
        <div className={classes.itemContent}>
          <CaseDocuments cases={cases} />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
