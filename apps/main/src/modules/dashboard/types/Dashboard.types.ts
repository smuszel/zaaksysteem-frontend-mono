export type DashboardCaseType = {
  id: string;
  caseType: string;
  caseNumber: number;
  requestor: {
    id: string;
    name: string;
  };
};
