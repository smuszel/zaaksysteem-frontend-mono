import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const formDefinition = [
  {
    name: 'reason',
    type: TEXT,
    value: '',
    required: true,
    label: 'caseTypeVersions:fields.reason.label',
    placeholder: 'caseTypeVersions:fields.reason.label',
  },
];

export default formDefinition;
