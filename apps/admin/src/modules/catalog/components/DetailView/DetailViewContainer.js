import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { filterProperties } from '@mintlab/kitchen-sink/source';
import { toggleCatalogDetailView } from '../../store/details/details.actions';
import DetailView from './DetailView';

const mapDispatchToProps = dispatch => ({
  toggleDetailView: bindActionCreators(toggleCatalogDetailView, dispatch),
});

const mapStateToProps = ({
  catalog: {
    items: { currentFolderName, selectedItems, items },
    details: { data },
  },
}) => ({
  catalogItem: data,
  currentFolderName,
  selectedItems,
  items,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { t, rows } = ownProps;
  const { catalogItem, currentFolderName, selectedItems, items } = stateProps;

  const selectedRow = rows.find(row => row.id === selectedItems[0]) || {};
  const selectedCount = items.length ? selectedItems.length : 0;

  const title = t('catalog:title');

  const detailInfoConfig = {
    2: {
      name: `${selectedCount} ${t('catalog:detailView:itemsSelected')}`,
      icon: 'select_all',
    },
    1: {
      ...filterProperties(selectedRow, 'name', 'icon'),
      ...filterProperties(catalogItem, 'version', 'type', 'details'),
    },
    0: {
      name: currentFolderName || title,
      icon: 'folder',
    },
  };
  const detailInfo = detailInfoConfig[Math.min(selectedCount, 2)];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    detailInfo,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DetailView);
