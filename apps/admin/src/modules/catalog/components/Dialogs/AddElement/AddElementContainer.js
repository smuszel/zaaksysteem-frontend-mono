import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { invoke } from '../../../../../store/route/route.actions';
import { initEmailTemplate } from '../../../store/emailTemplate/emailTemplate.actions';
import { initAttribute } from '../../../store/attribute/attribute.actions';
import { initFolder } from '../../../store/folder/folder.actions';
import { initDocumentTemplate } from '../../../store/documentTemplate/documentTemplate.actions';
import {
  getPathToCaseType,
  getPathToObjectType,
  getPathToImport,
  getPathToCustomObjectType,
} from '../../../library/pathGetters';
import { hideDialog } from '../../../../../store/ui/ui.actions';
import AddElement from './AddElement';

/**
 * @param {Object} state
 * @param {Object} state.catalog
 * @param {Object} state.catalog.items
 * @param {Object} state.catalog.details
 * @return {Object}
 */
const mapStateToProps = ({
  catalog: {
    items: { currentFolderUUID },
  },
}) => ({
  currentFolderUUID,
});

const mapDispatchToProps = dispatch => {
  const doNavigate = path =>
    invoke({
      path,
    });
  const hideDialogAndDispatch = action => (...rest) => {
    dispatch(hideDialog());
    dispatch(action(...rest));
  };

  return {
    doNavigate: hideDialogAndDispatch(doNavigate),
    initEmailTemplateAction: hideDialogAndDispatch(initEmailTemplate),
    initAttributeAction: hideDialogAndDispatch(initAttribute),
    initFolderAction: hideDialogAndDispatch(initFolder),
    initDocumentTemplateAction: hideDialogAndDispatch(initDocumentTemplate),
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentFolderUUID } = stateProps;
  const {
    doNavigate,
    initEmailTemplateAction,
    initAttributeAction,
    initFolderAction,
    initDocumentTemplateAction,
  } = dispatchProps;
  const { t } = ownProps;

  const sections = [
    [
      {
        type: 'case_type',
        title: t('catalog:type:case_type'),
        action() {
          doNavigate(
            getPathToCaseType(0, '/bewerken', true, {
              folder_uuid: currentFolderUUID,
            })
          );
        },
      },
      {
        type: 'object_type',
        title: t('catalog:type:object_type'),
        action() {
          doNavigate(
            getPathToObjectType(0, '/bewerken', true, {
              folder_uuid: currentFolderUUID,
            })
          );
        },
      },
      {
        type: 'custom_object_type',
        title: t('catalog:type:custom_object_type'),
        action() {
          doNavigate(
            getPathToCustomObjectType({
              folder_uuid: currentFolderUUID,
            })
          );
        },
      },
      {
        type: 'email_template',
        title: t('catalog:type:email_template'),
        action() {
          initEmailTemplateAction();
        },
      },
      {
        type: 'document_template',
        title: t('catalog:type:document_template'),
        action() {
          initDocumentTemplateAction();
        },
      },
      {
        type: 'attribute',
        title: t('catalog:type:attribute'),
        action() {
          initAttributeAction();
        },
      },
    ],
    [
      {
        type: 'folder',
        title: t('catalog:type:folder'),
        action() {
          initFolderAction();
        },
      },
      {
        icon: 'publish',
        type: 'import',
        title: t('catalog:actions:import'),
        action() {
          doNavigate(getPathToImport());
        },
      },
    ],
  ];

  return {
    ...stateProps,
    ...ownProps,
    sections,
  };
};

const connectedDialog = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps, mergeProps)(AddElement)
);

export default connectedDialog;
