import React from 'react';
import { FormikValues } from 'formik';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { FormFields } from '@zaaksysteem/common/src/components/form/fields';
import {
  MAGICSTRING,
  PRODUCT_SELECT,
  LOCATION_SELECT,
  TYPES,
  OPTIONS,
  VERSIONS,
  NAME,
  TEXT,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  showFields,
  hideFields,
  valueEquals,
  valueOneOf,
  Rule,
} from '../../../../../components/Form/Rules';
import * as validationRules from '../../../../../components/Form/Validation/validationRules';
import Types from './Components/Types/Types';
import Versions from './Components/Versions/Versions';
import Options from './Components/Options/Options';
import Name from './Components/Name/Name';
import isValidMagicstring from './Validation/isValidMagicstring';
import LocationSelect from './Components/LocationSelect';
import ProductSelect from './Components/ProductSelect';

//Custom type mapping for the Attribute Dialog
const fieldComponents = {
  [TYPES]: Types,
  [OPTIONS]: Options,
  [VERSIONS]: Versions,
  [MAGICSTRING]: FormFields[TEXT],
  [NAME]: Name,
  [LOCATION_SELECT]: LocationSelect,
  [PRODUCT_SELECT]: ProductSelect,
};

// Custom Yup validation mapping for the Attribute Dialog
const validationMap = {
  [TYPES]: validationRules.selectRule,
  [MAGICSTRING]: isValidMagicstring,
  [LOCATION_SELECT]: validationRules.selectRule,
  [PRODUCT_SELECT]: validationRules.selectRule,
};

const rules: any = [
  new Rule('attribute_type')
    .when(valueOneOf(['checkbox', 'select', 'option']))
    .then(showFields(['attribute_values']))
    .else(hideFields(['attribute_values'])),
  new Rule('attribute_type')
    .when(valueEquals('file'))
    .then(
      showFields([
        'document_category',
        'document_trust_level',
        'document_origin',
      ])
    )
    .else(
      hideFields([
        'document_category',
        'document_trust_level',
        'document_origin',
      ])
    ),
  new Rule('attribute_type')
    .when(valueEquals('appointment'))
    .then(showFields(['appointment_location_id', 'appointment_product_id']))
    .else(hideFields(['appointment_location_id', 'appointment_product_id'])),
  new Rule('attribute_type')
    .when(valueOneOf(['numeric', 'select', 'text', 'text_uc', 'textarea']))
    .then(showFields(['type_multiple']))
    .else(hideFields(['type_multiple'])),
];

/**
 * @return {ReactElement}
 */
const Attribute: React.FunctionComponent<any> = ({
  saveAction,
  formDefinition,
  currentFolderUUID,
  appointment_interface_uuid,
  id,
  t,
  fetchingIntegrations,
  saving,
  loading,
  hide,
}) => {
  const handleOnSubmit = (values: FormikValues) =>
    saveAction({ values, id, currentFolderUUID, appointment_interface_uuid });

  const title = t('attribute:dialog.title', {
    action: id ? t('common:edit') : t('common:create'),
  });

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      title={title}
      scope="attribute-form-component"
      validationMap={validationMap}
      fieldComponents={fieldComponents}
      rules={rules}
      icon="extension"
      isInitialValid={id ? true : false}
      initializing={loading || fetchingIntegrations}
      saving={saving}
      onClose={hide}
    />
  );
};

export default Attribute;
