import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_EMAIL_TEMPLATE_INIT = 'CATALOG:EMAIL_TEMPLATE:INIT';
export const CATALOG_EMAIL_TEMPLATE_FETCH = createAjaxConstants(
  'CATALOG:EMAIL_TEMPLATE:FETCH'
);
export const CATALOG_EMAIL_TEMPLATE_SAVE = createAjaxConstants(
  'CATALOG:EMAIL_TEMPLATE:SAVE'
);
