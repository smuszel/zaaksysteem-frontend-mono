import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_FETCH = createAjaxConstants('CATALOG_FETCH');
export const CATALOG_TOGGLE_ITEM = 'CATALOG_TOGGLE_ITEM';
export const CATALOG_CLEAR_SELECTED = 'CATALOG_CLEAR_SELECTED';
