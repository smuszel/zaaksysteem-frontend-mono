import { navigation } from '../fixtures/navigation';

export const getSectionFromPath = path => {
  const found = navigation.find(item => path.includes(item.path));
  return found ? found.section : '';
};
