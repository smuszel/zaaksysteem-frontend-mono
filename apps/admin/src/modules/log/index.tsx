import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getLogModule } from './store/log.module';
import LogContainer from './components/LogContainer';

const LogModule: React.ComponentType<{}> = () => {
  return (
    <DynamicModuleLoader modules={[getLogModule()]}>
      <LogContainer />
    </DynamicModuleLoader>
  );
};

export default LogModule;
