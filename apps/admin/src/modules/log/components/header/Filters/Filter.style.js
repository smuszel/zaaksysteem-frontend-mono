/**
 * @return {JSS}
 */
export const filterStyleSheet = ({ breakpoints }) => ({
  filterWrapper: {
    margin: '0px 27px 0px 7px',
    flex: 1,
    [breakpoints.up('md')]: {
      width: '100%',
      minWidth: '200px',
      maxWidth: '300px',
    },
  },
});
