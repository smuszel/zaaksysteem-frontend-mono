import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getSystemConfigurationModule } from './store/systemconfiguration.module';
import SystemConfigurationContainer from './components/SystemConfigurationContainer';

const SystemConfigurationModule: React.ComponentType<any> = props => {
  return (
    <DynamicModuleLoader modules={[getSystemConfigurationModule()]}>
      <SystemConfigurationContainer {...props} />
    </DynamicModuleLoader>
  );
};

export default SystemConfigurationModule;
