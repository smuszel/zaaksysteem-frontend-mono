export interface ObjectTypeManagementRouteType {
  segments: ['create' | 'edit', string];
  params: { folder_uuid?: string };
}
