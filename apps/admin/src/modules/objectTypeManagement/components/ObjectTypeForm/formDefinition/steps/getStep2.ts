import i18next from 'i18next';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { ATTRIBUTE_SELECT } from '../../../../../../components/Form/Constants/fieldTypes';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getStep2(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.2.title'),
    fields: [
      {
        name: 'custom_fields',
        type: ATTRIBUTE_SELECT,
        value: [],
        required: true,
        placeholder: t(
          'objectTypeManagement:form.fields.custom_fields.placeholder'
        ),
      },
    ],
  };
}
