import React from 'react';
import { useObjectTypeStyle } from './ObjectTypeManagement.style';
import ObjectTypeCreate from './Create/ObjectTypeCreate';
import ObjectTypeEdit from './Edit/ObjectTypeEdit';

export type ObjectTypeManagementType = {
  type: 'create' | 'edit';
  uuid: string;
  folderUuid?: string;
};

const ObjectTypeManagement: React.ComponentType<ObjectTypeManagementType> = ({
  type,
  uuid,
  folderUuid,
}) => {
  const classes = useObjectTypeStyle();

  return (
    <div className={classes.wrapper}>
      {type === 'create' ? (
        <ObjectTypeCreate folderUuid={folderUuid} />
      ) : (
        <ObjectTypeEdit uuid={uuid} />
      )}
    </div>
  );
};

export default ObjectTypeManagement;
