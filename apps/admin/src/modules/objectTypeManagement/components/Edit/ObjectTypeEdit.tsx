import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { PartialFormValuesType } from '@zaaksysteem/common/src/components/form/types';
import ObjectTypeForm from '../ObjectTypeForm/ObjectTypeForm';
import { objectTypeSelector } from '../../store/selectors/objectTypeSelector';
import { ObjectTypeFormShapeType } from '../ObjectTypeForm/ObjectTypeForm.types';
import { updateObjectTypeAction } from '../../store/update/update.actions';
import { busyUpdatingSelector } from '../../store/selectors/busyUpdatingSelector';
import { unsavedChangesSelector } from '../../store/selectors/unsavedChangesSelector';
import { setObjectTypeUnsavedChangesAction } from '../../store/unsavedChanges/unsavedChanges.actions';
import { fetchObjectTypeAction } from './../../store/objectType/objectType.actions';
import { useObjectTypeEditStyle } from './ObjectTypeEdit.style';

export type ObjectTypeEditPropsType = {
  uuid: string;
};

const ObjectTypeEdit: React.ComponentType<ObjectTypeEditPropsType> = ({
  uuid,
}) => {
  const classes = useObjectTypeEditStyle();
  const { state, objectType } = useSelector(objectTypeSelector);
  const unsavedChanges = useSelector(unsavedChangesSelector);
  const busyUpdating = useSelector(busyUpdatingSelector);
  const dispatch = useDispatch();

  const handleSubmit = (values: ObjectTypeFormShapeType) =>
    dispatch(updateObjectTypeAction({ ...values, uuid }));

  const handleChange = () =>
    !unsavedChanges && dispatch(setObjectTypeUnsavedChangesAction(true));

  useEffect(() => {
    dispatch(fetchObjectTypeAction(uuid));
  }, []);

  if (state === 'pending' || !objectType) {
    return <Loader delay={200} />;
  }
  const { name, title } = objectType;
  const initialValues: PartialFormValuesType<ObjectTypeFormShapeType> = {
    name,
    title,
  };

  return (
    <div className={classes.wrapper}>
      <ObjectTypeForm
        busy={busyUpdating}
        onChange={handleChange}
        onSubmit={handleSubmit as any}
        initialValues={initialValues}
      />
    </div>
  );
};

export default ObjectTypeEdit;
