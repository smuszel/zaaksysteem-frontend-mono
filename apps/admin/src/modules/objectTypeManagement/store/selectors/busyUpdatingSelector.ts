import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';

export const busyUpdatingSelector = (
  state: ObjectTypeManagementRootStateType
): boolean => state.objectTypeManagement.update.state === 'pending';
