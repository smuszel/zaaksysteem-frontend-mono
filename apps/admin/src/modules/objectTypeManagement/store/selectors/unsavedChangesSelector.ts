import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';
import { UnsavedChangesStateType } from '../unsavedChanges/unsavedChanges.reducer';

export const unsavedChangesSelector = (
  state: ObjectTypeManagementRootStateType
): UnsavedChangesStateType => state.objectTypeManagement.unsavedChanges;
