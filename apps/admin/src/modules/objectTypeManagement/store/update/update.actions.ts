//@ts-ignore
import uuidv4 from 'uuid/v4';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ObjectTypeFormShapeType } from '../../components/ObjectTypeForm/ObjectTypeForm.types';
import { OBJECT_TYPE_UPDATE } from './update.constants';

const updateObjectTypeAjaxAction = createAjaxAction(OBJECT_TYPE_UPDATE);

export type UpdateObjectTypePayloadType = ObjectTypeFormShapeType & {
  folderUuid?: string;
  uuid: string;
};

export const updateObjectTypeAction = (
  payload: UpdateObjectTypePayloadType
) => {
  const url = buildUrl<APICaseManagement.UpdateCustomObjectTypeRequestParams>(
    '/api/v2/cm/custom_object_type/update_custom_object_type',
    {
      uuid: payload.uuid,
    }
  );
  const data: APICaseManagement.UpdateCustomObjectTypeRequestBody = {
    uuid: uuidv4(),
    title: payload.title,
    name: payload.name,
    status: 'active',
  };

  return updateObjectTypeAjaxAction({
    url,
    method: 'POST',
    data,
    payload,
  });
};
