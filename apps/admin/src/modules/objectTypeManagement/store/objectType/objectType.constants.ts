import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const OBJECT_TYPE_FETCH = createAjaxConstants('OBJECT_TYPE_FETCH');
