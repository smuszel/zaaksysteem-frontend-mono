import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OBJECT_TYPE_FETCH } from './objectType.constants';

const fetchObjectTypeAjaxAction = createAjaxAction(OBJECT_TYPE_FETCH);

export const fetchObjectTypeAction = (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    '/api/v2/cm/custom_object_type/get_custom_object_type',
    {
      uuid,
    }
  );

  return fetchObjectTypeAjaxAction({
    url,
    method: 'GET',
  });
};
