import React, { Suspense } from 'react';
import { Provider } from 'react-redux';
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import ErrorBoundary from './components/App/ErrorBoundary';
import LocaleContainer from './components/App/LocaleContainer';
import LoginContainer from './components/App/LoginContainer';
import LayoutContainer from './components/App/LayoutContainer';
import RouterContainer from './components/App/RouterContainer';
import { configureStore } from './configureStore';
import { onPopState } from './library/dom/history';
import { getUrl } from './library/url';
import { resolve } from './store/route/route.actions';
import { init } from './store/app/app.actions';
import routes from './routes';
import './App.css';

const initialState = {};
const store = configureStore(initialState);

function dispatchRoute() {
  store.dispatch(
    resolve({
      path: getUrl(),
    })
  );
}

store.dispatch(init());

onPopState(dispatchRoute);

const AdminApp = () => (
  <Provider store={store}>
    <MaterialUiThemeProvider>
      <Suspense fallback={<Loader delay={200} />}>
        <ErrorBoundary>
          <LocaleContainer>
            <LoginContainer>
              <LayoutContainer>
                <RouterContainer routes={routes} />
              </LayoutContainer>
            </LoginContainer>
          </LocaleContainer>
        </ErrorBoundary>
      </Suspense>
    </MaterialUiThemeProvider>
  </Provider>
);

export default AdminApp;

if (module.hot) {
  module.hot.accept();
}
