/**
 * @param {Object} theme
 * @return {JSS}
 */
export const coloredIconStyleSheet = ({ palette: { vivid } }) => ({
  case_type: {
    color: vivid['4'],
  },
  email_template: {
    color: vivid['5'],
  },
  object_type: {
    color: vivid['3'],
  },
  custom_object_type: {
    color: vivid['3'],
  },
  folder: {
    color: vivid['2'],
  },
  document_template: {
    color: vivid['6'],
  },
  attribute: {
    color: vivid['1'],
  },
  select_all: {
    color: vivid['5'],
  },
});
