import React from 'react';
//@ts-ignore
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';
import { DialogPropsType } from '../../DialogRenderer/DialogRenderer.types';

const DiscardChangesDialog: React.ComponentType<DialogPropsType> = ({
  t,
  hide,
  options,
  invoke,
}) => (
  <Alert
    open={true}
    title={t('dialog:discardChanges:title')}
    icon="alarm"
    primaryButton={{
      text: t('dialog:ok'),
      onClick: () => {
        invoke({ ...options, force: true });
        hide();
      },
    }}
    secondaryButton={{
      text: t('dialog:cancel'),
      onClick: () => hide(),
    }}
    onClose={() => hide()}
  >
    {t('dialog:discardChanges:text')}
  </Alert>
);

export default DiscardChangesDialog;
