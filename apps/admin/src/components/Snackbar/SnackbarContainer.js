import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { hideSnackbar } from '../../store/ui/ui.actions';
import SnackbarWrapper from './Snackbar';

const mapStateToProps = ({ ui: { snackbar } }) => ({ snackbar });
const mapDispatchToProps = dispatch => ({
  onQueueEmpty: payload => dispatch(hideSnackbar(payload)),
});

const SnackbarContainer = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(SnackbarWrapper)
);

export default SnackbarContainer;
