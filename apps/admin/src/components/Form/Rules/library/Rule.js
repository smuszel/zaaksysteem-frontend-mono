const triggerActions = (actions, fields) =>
  Array.isArray(actions)
    ? actions.reduce((thisFields, action) => action(thisFields), fields)
    : actions(fields);

export class Rule {
  ifFn = null;
  thenFn = [];
  elseFn = [];
  activeAction = null;

  constructor(fieldName) {
    this.fieldName = fieldName;
  }

  when(fn) {
    this.ifFn = fn;
    return this;
  }

  then(fn) {
    this.activeAction = 'then';
    this.thenFn.push(fn);
    return this;
  }

  else(fn) {
    this.activeAction = 'else';
    this.elseFn.push(fn);
    return this;
  }

  and(fn) {
    switch (this.activeAction) {
      case 'then':
        return this.then(fn);

      case 'else':
        return this.else(fn);

      default:
        throw new Error(
          'Cannot call and(...) before calling then(...) or when(...)'
        );
    }
  }

  execute(fields) {
    const field = fields.find(item => item.name === this.fieldName);
    const value = field ? field.value : null;

    if (this.ifFn(value, field)) {
      return triggerActions(this.thenFn, fields);
    } else if (this.elseFn) {
      return triggerActions(this.elseFn, fields);
    }

    return fields;
  }
}

export default Rule;
