import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
// @ts-ignore
import { GenericTextField } from '@mintlab/ui/App/Material/TextField';
import { useAttributeListItemStyle } from './AttributeItem.style';
import { AttributeItemType } from './types';

export const AttributeItem: React.ComponentType<AttributeItemType & {
  onUpdate: (nextItem: AttributeItemType, index: number) => void;
  onRemove: (index: number) => void;
  index: number;
}> = ({ name, id, onUpdate, onRemove, index }) => {
  const classes = useAttributeListItemStyle();
  const [t] = useTranslation('objectTypeManagement');

  return (
    <div className={classes.mainContainer}>
      <div className={classes.headerContainer}>
        <h5 className={classes.attributeName}>{name}</h5>
        <div className={classes.actionsContainer}>
          <Button onClick={() => {}} presets={['default', 'link']}>
            Instellen
          </Button>
          <Button onClick={() => onRemove(index)} presets={['default', 'icon']}>
            close
          </Button>
        </div>
      </div>
      <GenericTextField
        placeholder={t(
          'objectTypeManagement:form.fields.custom_fields.titlePlaceholder'
        )}
        onBlur={(event: any) =>
          onUpdate({ name, id, title: event.target.value }, index)
        }
      />
    </div>
  );
};
