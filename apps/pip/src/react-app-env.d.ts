/// <reference types="react-scripts" />

declare namespace NodeJS {
  interface ProcessEnv {
    readonly WEBPACK_BUILD_TARGET: 'development' | 'production';
  }
}
